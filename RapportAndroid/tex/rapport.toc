\select@language {french}
\contentsline {chapter}{\numberline {1}Introduction : pr\IeC {\'e}sentation du projet}{2}{chapter.1}
\contentsline {paragraph}{Le Principe est le suivant:}{2}{section*.2}
\contentsline {paragraph}{}{2}{section*.3}
\contentsline {paragraph}{}{3}{section*.4}
\contentsline {chapter}{\numberline {2}Architecture de l'application}{4}{chapter.2}
\contentsline {paragraph}{}{5}{section*.5}
\contentsline {chapter}{\numberline {3}Application Android}{6}{chapter.3}
\contentsline {section}{\numberline {3.1}Construction de l'interface}{6}{section.3.1}
\contentsline {paragraph}{}{6}{section*.6}
\contentsline {section}{\numberline {3.2}Gestion de la rotation}{7}{section.3.2}
\contentsline {paragraph}{}{7}{section*.7}
\contentsline {section}{\numberline {3.3}Lien avec le moteur}{8}{section.3.3}
\contentsline {chapter}{\numberline {4}Interface web mobile}{9}{chapter.4}
\contentsline {section}{\numberline {4.1}Pr\IeC {\'e}sentation de l'interface web mobile}{9}{section.4.1}
\contentsline {section}{\numberline {4.2}G\IeC {\'e}n\IeC {\'e}ration des requ\IeC {\^e}tes HTTP avec CGI}{9}{section.4.2}
\contentsline {section}{\numberline {4.3}Structure des pages CGI}{10}{section.4.3}
\contentsline {paragraph}{}{10}{section*.8}
\contentsline {paragraph}{}{10}{section*.9}
\contentsline {chapter}{\numberline {5}Conclusion}{12}{chapter.5}
\contentsline {paragraph}{}{12}{section*.10}
\contentsline {paragraph}{}{12}{section*.11}
\contentsline {paragraph}{}{12}{section*.12}
\contentsline {paragraph}{}{12}{section*.13}
