// On inclue le header de la classe
#include "Piece.hpp"
#include <sstream>
#include <iostream>
#include <cstdlib>
#include "Parser.hpp"

using namespace std;

string Piece::toString() {
	unsigned int i;
	string str;
	ostringstream s;
	s << "piece " << idPiece << " nom : " << nom
			<< " : nbModule = " << nbModule
			<< " : nbAngle = " << nbAngle << endl;
	str = s.str();
	for (i=0; i < ListMod.size(); i++) {

		str += ListMod[i]->toString();

	}
	return str;

}

Piece::Piece(int id, string nom, int nbMod,int nbAng){
	idPiece=id;
	this->nom = nom;
	nbModule=nbMod;
	nbAngle=nbAng;
	loadedModules = false;
}

Piece::Piece(int id, string nom, int nbMod,int nbAng, bool loadedModules, vector<Module*> ListMod) {
	this->idPiece=id;
	this->nom = nom;
	this->nbModule=nbMod;
	this->nbAngle=nbAng;
	this->loadedModules = loadedModules;
	this->ListMod = ListMod;
}

Piece::~Piece(){
	ListMod.~vector();
}

string Piece::getNom(){return nom;}

int Piece::getIdPiece(){return idPiece;}

int Piece::getNbModule(){return nbModule;}

int Piece::getNbAngle(){return nbAngle;}

Piece Piece::operator = (const Piece & piece){
	idPiece=piece.idPiece;
	nbModule=piece.nbModule;
	nbAngle=piece.nbAngle;
	return *this;
}

Module * Piece::getModuleByIndex(int idMod){

	if(!loadedModules) {

		ListMod = Parser::get()->getModules(idPiece);
		nbModule = ListMod.size();
		loadedModules = true;

	}

	switch(ListMod[idMod]->getType()){
	case TYPE_PRISE:
		return (Prise*)ListMod[idMod];
		break;
	case TYPE_LUMIERE:
		return (Lumiere*)ListMod[idMod];
		break;
	case TYPE_VOLET:
		return (Volet*)ListMod[idMod];
		break;
	default:
		return NULL;
		break;
	}
	return NULL;
}

void Piece::actualise() {
	ListMod = Parser::get()->getModules(idPiece);
	nbModule = ListMod.size();
	loadedModules = true;
}

string Piece::serialize() {

	unsigned int i;
	ostringstream ss;

	ss << idPiece << endl
		<< nom << endl
		<< nbModule << endl
		<< nbAngle << endl
		<< loadedModules << endl
		<< ListMod.size() << endl;

	for(i=0; i < ListMod.size(); i++) {

		ss << ListMod[i]->serialize();

	}

	return ss.str();

}

Piece* Piece::unserialize(ifstream& file) {

	bool loadedModules;
	unsigned int idPiece, nbModule, nbAngle, nbMod, i, type;
	string nom;
	vector<Module*> ListMod;

	file >> idPiece;
	getline(file,nom);
	getline(file,nom);
	file >> nbModule
		>> nbAngle
		>> loadedModules
		>> nbMod;

	for(i=0; i < nbMod; i++) {
		file >> type;
		switch(type) {

		case TYPE_PRISE :

			ListMod.push_back(Prise::unserialize(file));

			break;

		case TYPE_LUMIERE :

			ListMod.push_back(Lumiere::unserialize(file));

			break;

		case TYPE_VOLET :

			ListMod.push_back(Volet::unserialize(file));

			break;

		default :

			cout << "Error in Piece::unserialize() : wrong module type" << endl;
			exit(EXIT_FAILURE);

		}

	}

	return new Piece(idPiece, nom, nbModule, nbAngle, loadedModules, ListMod);

}
