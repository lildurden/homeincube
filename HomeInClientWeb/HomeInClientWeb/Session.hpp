/*
 * Session.h
 *
 *  Created on: 11 janv. 2012
 *      Author: foxnounours
 */

#ifndef SESSION_H_
#define SESSION_H_

#define _SESSION_DIR "/var/www/session/"
#define _SESSION_EXPIRATION 2700
#define _SESSION_BETEWEEN_CONNEXIONS 86400

#include <string>
#include <time.h>
#include "Moteur2Interface.hpp"

class Session {

private:

	std::string id;
	time_t timeCreated;
	time_t timeLastAccess;
	Moteur2Interface* m2i;

	void createSession();
	bool loadSession(std::string id);

public:

	Session();
	Session(std::string id);
	Moteur2Interface* getMoteur2Interface();
	std::string getId();
	~Session();

};

#endif /* SESSION_H_ */
