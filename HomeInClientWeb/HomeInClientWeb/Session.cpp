/*
 * Session.cpp
 *
 *  Created on: 11 janv. 2012
 *      Author: foxnounours
 */

#include "Session.hpp"
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstdlib>

using namespace std;

Session::Session() {

	createSession();

}

Session::Session(string id) {

	if(!loadSession(id))

		createSession();

}

void Session::createSession() {

	ostringstream ss;
	string ipUser = "127.168.1.1";//getenv("REMOTE_ADDR");

	ss << "ip" << ipUser << "time" << time(NULL);

	id = ss.str();
	timeCreated = time(NULL);
	m2i = new Moteur2Interface();

}

bool Session::loadSession(std::string id) {

	string s;
	string fileName = _SESSION_DIR + id;
	time_t old, timeSinceLastAccess;

	ifstream file(fileName.c_str(), ios::in);

	if(!file) {

		return false;

	}

	// On recupere l'age de la session
	file >> timeCreated;
	old = time(NULL) - timeCreated;

	if(old > _SESSION_EXPIRATION) {

		return false;

	}

	// On recupere la date de la derniere connexion
	file >> timeLastAccess;
	timeSinceLastAccess = time(NULL) - timeLastAccess;

	if(timeSinceLastAccess > _SESSION_BETEWEEN_CONNEXIONS) {

		return false;

	}

	// On recupere le moteur
	m2i = Moteur2Interface::unserialize(file);

	file.close();
	this->id = id;

	return true;
}

Moteur2Interface* Session::getMoteur2Interface() {

	return m2i;

}

string Session::getId() {

	return id;

}

Session::~Session() {

	string fileName = _SESSION_DIR + id;

	ofstream file(fileName.c_str(), ios::out | ios::trunc);
	if(!file) {

		cout << "error in SESSION::~SESSION() : impossible d'enregistrer les données dans la session." << endl;
		exit(EXIT_FAILURE);

	}

	file << timeCreated << endl;

	file << time(NULL) << endl;

	file << m2i->serialize() << endl;

	file.close();

}

