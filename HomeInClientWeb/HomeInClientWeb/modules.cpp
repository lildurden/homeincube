/*
 * modules.cpp
 *
 *  Created on: 13 janv. 2012
 *      Author: foxnounours
 */
#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include "Session.hpp"
#include "Moteur2Interface.hpp"
#include "HTML.hpp"
#include "REQUETE.hpp"

using namespace std;

int main(int argc, char** arg) {

	ostringstream outs, outs2;
	Session* session;
	Moteur2Interface* m2i;
	HTML* page = new HTML("HomeInCube : Liste des modules");
	REQUETE* requete = new REQUETE();
	int i, nbModules, idPiece;

	if(requete->existGetVar("session")) {

		session = new Session(requete->getGetVar("session"));

	} else {

		session = new Session();

	}

	if(requete->existGetVar("piece")) {

		idPiece = atoi(requete->getGetVar("piece").c_str());

	} else {

		cout << "Location: pieces.cgi?session=" << session->getId() << endl << endl;
		exit(0);

	}

	m2i = session->getMoteur2Interface();

	// On se connecte
	if(!m2i->connect("127.0.0.1", 9000)) {

		cout << "Content-Type: text/html;" << endl << endl
			<< "erreur serveur indisponible" << endl;
		exit(EXIT_SUCCESS);

	}

	outs2 << "<div data-role=\"page\" data-theme=\"b\">"
		"<div data-role=\"header\">"
		"<a data-ajax=\"false\" href=\"pieces.cgi?session=" + session->getId() + "\">retour</a>"
		"<h1>Home In Cube</h1>"
		"<a data-ajax=\"false\" href=\"actualise.cgi?piece=" << idPiece << "\">actualise</a>"
		"</div>"
		"<div data-role=\"content\">"
		"<ul data-role=\"listview\" data-inset=\"true\" data-dividertheme=\"b\">"
		"<li data-role=\"list-divider\">Liste des modules de ";
	page->addHtmlContent(outs2.str());
	page->addTxtContent(m2i->getNomPiece(idPiece));
	page->addHtmlContent("</li>");

	// On récupere la liste des module
	nbModules = m2i->getNbModule(idPiece);

	for(i=0; i < nbModules; i++) {

		outs << "<li>"
			<< "<h3>"
			<< "<a data-ajax=\"false\" href=\"commandes.cgi?session=" << session->getId() << "&piece=" <<  idPiece << "&module=" << i << "\">"
			<< m2i->getNomModule(idPiece, i)
			<< "</a>"
			<< "</h3>"
			<< "</li>";

	}

	page->addHtmlContent(outs.str());

	page->addHtmlContent("</ul>"
							"</div> <!-- content -->"
							"<div data-role=\"footer\" data-position=\"fixed\">"
							"Fin de liste"
							"</div>"
							"</div> <!-- page -->");

	page->output();

	session->~Session();

	return 0;

}
