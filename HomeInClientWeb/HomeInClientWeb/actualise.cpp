/*
 * actualise.cpp
 *
 *  Created on: 16 janv. 2012
 *      Author: foxnounours
 */
#include <iostream>
#include <cstdlib>
#include "REQUETE.hpp"
#include "Session.hpp"

using namespace std;

int main(int argc, char** arg) {

	REQUETE* requete = new REQUETE();
	Session* session = new Session();
	int idPiece, idModule;

	if(!requete->existGetVar("piece")) {

		cout << "Location: pieces.cgi?session=" << session->getId() << endl << endl;

	} else {

		idPiece = atoi(requete->getGetVar("piece").c_str());

		if(!requete->existGetVar("module")) {

		cout << "Location: modules.cgi?session=" << session->getId() << "&piece=" << idPiece << endl << endl;

		} else {

			idModule = atoi(requete->getGetVar("module").c_str());
			cout << "Location: commandes.cgi?session=" << session->getId() << "&piece=" << idPiece
					<< "&module=" << idModule << endl << endl;
		}

	}

	session->getMoteur2Interface()->connect("127.0.0.1", 9000);

	session->~Session();

	return 0;

}
