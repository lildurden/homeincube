#include "Volet.hpp"
#include <sstream>
#include "Parser.hpp"

using namespace std;


Volet::Volet (int idC, string nom, int idPieceC, bool etatC,int progressionC): Module(idC, nom, idPieceC, etatC){
	progression = progressionC;
}

int Volet::getProgression(){
	return progression;
}


void Volet::setProgression( int value){
	progression = value;
	Parser::get()->setModule(this);
}

void Volet::setAll(bool etat, int value){
	this->setEtatAlone(etat);
	progression = value;
	Parser::get()->setModule(this);
}

int Volet::getType() {
	return TYPE_VOLET;
}

string Volet::toString() {
	ostringstream s;
	s << "Volet " << getId() << " : "
			<< getNom() << " est dans la piece "
			<< getIdPiece() << " et sont état est "
			<< getEtat()
			<< " progression  : " << progression << endl;
	return s.str();
}

Volet::~Volet() {

}

std::string Volet::serialize() {

	ostringstream ss;

	ss << getType() << endl
		<< getId() << endl
		<< getNom() << endl
		<< getIdPiece() << endl
		<< getEtat() << endl
		<< progression << endl;

	return ss.str();

}

Volet* Volet::unserialize(std::ifstream& file) {

	bool etat;
	unsigned int id, idPiece, progression;
	string nom;

	file >> id;
	getline(file,nom);
	getline(file,nom);
	file >> idPiece
		>> etat
		>> progression;

	return new Volet(id, nom, idPiece, etat, progression);

}
