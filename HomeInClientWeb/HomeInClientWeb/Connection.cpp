/*
 * connection.cpp
 *
 *  Created on: 5 janv. 2012
 *      Author: foxnounours
 */

#include "Connection.hpp"
#include <sstream>
#include <iostream>
#include "TCP.hpp"

using namespace std;

Connection* Connection::unserialize(ifstream& file) {

	string address;
	int port;

	file >> address;
	file >> port;

	return new Connection(address, port);

}

std::string Connection::serialize() {

	ostringstream ss;

	ss << adresse << endl
		<< port << endl;

	return ss.str();

}

Connection::Connection(string adresse, int port) {

	this->adresse = adresse;
	this->port = port;

}

bool Connection::connect() {

	return TCP::get()->connexion(adresse.c_str(), port);

}

