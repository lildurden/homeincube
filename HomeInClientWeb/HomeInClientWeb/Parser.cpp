// On inclue le header de la classe
#include "Parser.hpp"
#include <iostream>
#include <cstdlib>
#include <sstream>
#include "TCP.hpp"

using namespace std;

// On definit les attributs static de la classe
// ainsi que leur valeur par défaut
bool Parser::instancied = false;
Parser * Parser::parser;

/**
 * Methode qui convertie une string en int
 */
int parseInt(string s) {

	istringstream ss(s);
	int i;
	if(!(ss >> i)) {

		cout << "Error in parseInt(string) : string incorrecte"
				<< endl;
		exit(EXIT_FAILURE);

	}

	return i;

}

/**
 * Methode qui convertie une string en bool
 */
bool parseBool(string s) {

	istringstream ss(s);
	bool b;
	if(!(ss >> b)) {

		cout << "Error in parseBool(string) : string incorrecte"
				<< endl;
		exit(EXIT_FAILURE);

	}

	return b;

}

/**
 * Methode qui convertie un int en string
 */
string toString(int i) {

	ostringstream ss;
	ss << i;

	return ss.str();

}

/**
 * Methode qui convertie un bool en string
 */
string toString(bool b) {

	ostringstream ss;
	ss << b;

	return ss.str();

}

string* Parser::split(string str2split, int* tSize) {

	// On déclare les variables locales
	unsigned int i, j = 0, taille = 0;
	int niveau = 0;
	string strAux;
	string *strTable;

	// On calcule la taille du tableau et on la recupere
	for (i = 0; i < str2split.size(); i++) {

		// Si on a un caractere ouvrant on augmente de niveau
		if ( str2split[i] == '<') {

			// Si le niveau est 0
			if(niveau == 0) {

				// On a un element de plus
				taille++;

			}

			niveau++;

		}
		// Si on a un caractere fermant on diminue de niveau
		else if ( str2split[i] == '>') {

			niveau--;

			// Si on arrive à un niveau negatif la trame est incorrecte
			if(niveau < 0) {

				cout << "Error in Parser::split(string, int*) : trame incorrecte" << endl;
				exit(EXIT_FAILURE);

			}

		}

	}

	// Si le niveau n'est pas égal a 0 à la fin on a une trame incorrecte
	if(niveau != 0) {

		cout << "Error in Parser::split(string, int*) : trame incorrecte" << endl;
		exit(EXIT_FAILURE);

	}

	// On enregistre la taille du tableau
	*tSize = taille;

	// On crée le tableau
	strTable = new string[taille];

	// On decoupe la trame
	for (i = 0; i < str2split.size(); i++) {

		// Si on a un caractere fermant on diminue de niveau
		if (str2split[i] == '>') {

			niveau--;

			// Si on est à la fin d'un element
			if(niveau == 0) {

				// On ajoute l'element et on reinitialise pour le suivant
				strTable[j] = strAux;
				j++;
				strAux = "";

			}

		}

		// Si c'est un autre caractere on l'ajoute a la chaine auxiliere
		if(niveau != 0){

			strAux += str2split[i];

		}

		// Si on a un caractere ouvrant on augmente de niveau
		if ( str2split[i] == '<') {

			niveau++;

		}

	}

	return strTable;

}

Parser* Parser::get() {

	if(!instancied) {

		instancied = true;
		parser = new Parser();

	}

	return parser;

}

bool Parser::connect(User* user) {

	int nbArgs;
	string trame;
	string *connectArgs;

	trame = "<userConnection><"
				+ user->getNom() + "><"
				+ user->getMdp() + ">";

	TCP::get()->sended(trame.c_str());

	trame = TCP::get()->recieve();
	connectArgs = split(trame, &nbArgs);
	return parseBool(connectArgs[1]);

}

vector<Piece*> Parser::getPieces() {

	// On déclare les variables locales
	string trame;
	string *trameArgs, *listArgs, *pieceArgs;
	int nbTrameArg, nbListArg, nbPieceArg, i,
	idTemp, nbModuleTemp, nbAngleTemp;
	vector<Piece*> pieces;

	// On demande au serveur la liste des pieces
	TCP::get()->sended("<getPieces>");

	// On recois la trame
	trame = TCP::get()->recieve();

	// On decoupe la trame
	trameArgs = split(trame, &nbTrameArg);

	// Si la reponse n'est pas celle attendu
	if(trameArgs[0] != "getPieces") {

		cout << "Error in Parser::getPieces() : reponse reçu incorrecte" << endl;
		exit(EXIT_FAILURE);

	} else if(nbTrameArg != 3) {

		cout << "Error in Parser::getPieces() : nombre d'argument de la reponse incorrecte" << endl;
		exit(EXIT_FAILURE);

	}

	// On decoupe les elements de la liste
	listArgs = split(trameArgs[2], &nbListArg);

	// Pour chaque element de la liste de piece
	for(i = 0; i < nbListArg; i++) {

		// On decoupe les elements de la piece
		pieceArgs = split(listArgs[i], &nbPieceArg);

		// Si le nombre d'argument est incorrecte
		if(nbPieceArg != 4) {

			cout << "Error in Parser::getPieces() : nombre d'argument pour la piece "
					<< toString(i)
					<< " incorrecte"
					<< endl;

			exit(EXIT_FAILURE);

		}

		// On convertie les arguments
		idTemp = parseInt(pieceArgs[0]);
		nbModuleTemp = parseInt(pieceArgs[2]);
		nbAngleTemp = parseInt(pieceArgs[3]);

		// On cree et on ajoute la piece au vector
		pieces.push_back(new Piece(idTemp, pieceArgs[1], nbModuleTemp, nbAngleTemp));

	}

	// On retourne le vector
	return pieces;

}

vector<Module*> Parser::getModules(int idPiece) {

	// On déclare les variables locales
	string trame, s;
	string *trameArgs, *listArgs, *moduleArgs;
	int nbTrameArg, nbListArg, nbModuleArg, i,
	idTemp, typeTemp, dimTemp, brightTemp, progressionTemp;
	bool etatTemp;
	vector<Module*> modules;

	// On demande la liste des modules
	s = "<getModules><"+toString(idPiece)+">";
	TCP::get()->sended(s.c_str());

	// On recupere la list des modules
	trame = TCP::get()->recieve();

	// On decoupe la trame
	trameArgs = split(trame, &nbTrameArg);

	// Si la reponse n'est pas celle attendu
	if(trameArgs[0] != "getModules") {

		cout << "Error in Parser::getModules() : reponse reçu incorrecte" << endl;
		exit(EXIT_FAILURE);

	} else if(nbTrameArg != 4) {

		cout << "Error in Parser::getModules() : nombre d'argument de la reponse incorrecte" << endl;
		exit(EXIT_FAILURE);

	}

	// On decoupe les elements de la liste
	listArgs = split(trameArgs[3], &nbListArg);

	// Pour chaque element de la liste de module
	for(i = 0; i < nbListArg; i++) {

		// On decoupe les elements du module
		moduleArgs = split(listArgs[i], &nbModuleArg);

		// On convertie les arguments
		idTemp = parseInt(moduleArgs[0]);
		typeTemp = parseInt(moduleArgs[2]);
		etatTemp = parseBool(moduleArgs[3]);

		// En fonction du type de module
		switch(typeTemp) {

		case TYPE_PRISE :

			//Si on a pas le bon nombre d'argument
			if(nbModuleArg != 4){

				cout << "Error in Parser::getModules(int) : nombre d'argument incorrect" << endl;
				exit(EXIT_FAILURE);

			}

			// On cree et on ajoute la prise a la piece
			modules.push_back((Module*)(new Prise(idTemp, moduleArgs[1], idPiece, etatTemp)));

			break;

		case TYPE_LUMIERE :

			//Si on a pas le bon nombre d'argument
			if(nbModuleArg != 6){

				cout << "Error in Parser::getModules(int) : nombre d'argument incorrect" << endl;
				exit(EXIT_FAILURE);

			}

			// On convertie les arguments spécifiques à la lumiere
			dimTemp = parseInt(moduleArgs[4]);
			brightTemp = parseInt(moduleArgs[5]);

			// On cree et on ajoute la lumiere a la piece
			modules.push_back((Module*)(new Lumiere(idTemp, moduleArgs[1], idPiece, etatTemp, dimTemp, brightTemp)));

			break;

		case TYPE_VOLET :

			//Si on a pas le bon nombre d'argument
			if(nbModuleArg != 5){

				cout << "Error in Parser::getModules(int) : nombre d'argument incorrect" << endl;
				exit(EXIT_FAILURE);

			}

			// On converti l'argument spécifique au volet
			progressionTemp = parseInt(moduleArgs[4]);

			// On cree et on ajoute le volet a la piece
			modules.push_back((Module*)(new Volet(idTemp, moduleArgs[1], idPiece, etatTemp, progressionTemp)));

			break;

		default :
			cout << "Error in Parser::getModules(int) : "
			<< "invalide module type" << endl;
			exit(EXIT_FAILURE);

		}

	}

	return modules;

}

void Parser::setModule(Module* module){

	string s = "<setModule><"+
			toString(module->getIdPiece()) +
			"><" +
			toString(module->getId()) +
			"><" +
			toString(module->getEtat()) +
			">";

	// On envoie la trame
	TCP::get()->sended(s.c_str());

}

void Parser::setModule(Prise* prise){

	string s = "<setPrise><"+
			toString(prise->getIdPiece()) +
			"><" +
			toString(prise->getId()) +
			"><" +
			toString(prise->getEtat()) +
			">";

	// On envoie la trame
	TCP::get()->sended(s.c_str());

}

void Parser::setModule(Lumiere* lumiere){

	string s = "<setLumiere><"+
			toString(lumiere->getIdPiece()) +
			"><" +
			toString(lumiere->getId()) +
			"><" +
			toString(lumiere->getEtat()) +
			"><" +
			toString(lumiere->getDim()) +
			"><" +
			toString(lumiere->getBright()) +
			">";

	// On envoie la trame
	TCP::get()->sended(s.c_str());

}

void Parser::setModule(Volet* volet){

	string s ="<setVolet><"+
			toString(volet->getIdPiece()) +
			"><" +
			toString(volet->getId()) +
			"><" +
			toString(volet->getEtat()) +
			"><" +
			toString(volet->getProgression()) +
			">";

	// On envoie la trame
	TCP::get()->sended(s.c_str());

}
