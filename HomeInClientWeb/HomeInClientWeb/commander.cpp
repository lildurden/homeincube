/*
 * modules.cpp
 *
 *  Created on: 13 janv. 2012
 *      Author: foxnounours
 */
#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include "Session.hpp"
#include "Moteur2Interface.hpp"
#include "HTML.hpp"
#include "REQUETE.hpp"

using namespace std;

int main(int argc, char** arg) {

	ostringstream outs;
	string temp, bright, dim;
	Session* session;
	Moteur2Interface* m2i;
	REQUETE* requete = new REQUETE();
	int idModule, idPiece;
	bool etat;

	if(requete->existGetVar("session")) {

		session = new Session(requete->getGetVar("session"));

	} else {

		session = new Session();

	}

	if(requete->existGetVar("piece")) {

		idPiece = atoi(requete->getGetVar("piece").c_str());

	} else {

		cout << "Location: pieces.cgi?session=" << session->getId() << endl << endl;
		exit(0);

	}

	if(requete->existGetVar("module")) {

		idModule = atoi(requete->getGetVar("module").c_str());

	} else {

		cout << "Location: modules.cgi?session=" << session->getId()
				<< "&piece=" << idPiece << endl << endl;
		exit(0);

	}

	m2i = session->getMoteur2Interface();

	// On se connecte
	if(!m2i->connect("127.0.0.1", 9000)) {

		cout << "Content-Type: text/html;" << endl << endl
			<< "erreur serveur indisponible" << endl;
		exit(EXIT_SUCCESS);

	}


	temp = requete->getPost("etat");
	etat = (temp == "1");

	switch(m2i->getTypeModule(idPiece, idModule)) {

	case 1 :

		m2i->setEtatModule(idPiece, idModule, etat);

		break;

	case 2 :

		bright = requete->getPost("bright");
		dim = requete->getPost("dim");
		m2i->setLumiere(idPiece, idModule, etat, atoi(bright.c_str()), atoi(dim.c_str()));

		break;

	case 3 :

		temp = requete->getPost("progression");
		m2i->setVolet(idPiece, idModule, etat, atoi(temp.c_str()));

		break;

	}

	cout << "Location: commandes.cgi?session=" << session->getId()
					<< "&piece=" << idPiece
					<< "&module=" << idModule << endl << endl;

	session->~Session();

	return 0;

}
