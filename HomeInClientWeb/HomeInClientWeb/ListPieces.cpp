#include "ListPieces.hpp"
#include <sstream>
#include "Parser.hpp"

using namespace std;

ListPieces* ListPieces::instance;

bool ListPieces::instancied = false;

string ListPieces::toString() {

  unsigned int i;
  string s = "Liste des pieces :\n";

  for(i=0; i < pieces.size(); i++) {

    s += pieces[i]->toString() + "\n";

  }

  return s;

}

ListPieces* ListPieces::get() {

  if(!instancied) {

    instance = new ListPieces();

  }
  
  return instance;
  
}

Piece* ListPieces::getByIndex(int index) {

  return pieces[index];

}

unsigned int ListPieces::getNbPiece() {

	return pieces.size();

}

ListPieces::ListPieces() {

    instancied = true;
    pieces = Parser::get()->getPieces();

}

ListPieces::ListPieces(std::vector<Piece*> pieces) {

	this->pieces = pieces;
	instancied = true;

}

void ListPieces::actualise() {

    instancied = true;
    pieces.clear();
    pieces = Parser::get()->getPieces();

}

string ListPieces::serialize() {

	unsigned int i;
	ostringstream ss;

	ss << pieces.size() << endl;

	for(i=0; i < pieces.size(); i++) {

		ss << pieces[i]->serialize();

	}

	return ss.str();

}

void ListPieces::unserialize(std::ifstream& file) {

	unsigned int i, nbPieces;
	vector<Piece*> pieces;

	file >> nbPieces;

	for(i=0; i < nbPieces; i++) {

		pieces.push_back(Piece::unserialize(file));

	}

	instance = new ListPieces(pieces);

}
