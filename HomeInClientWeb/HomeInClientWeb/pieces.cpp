/*
 * pieces.cpp
 *
 *  Created on: 13 janv. 2012
 *      Author: foxnounours
 */
#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include "Session.hpp"
#include "Moteur2Interface.hpp"
#include "HTML.hpp"
#include "REQUETE.hpp"

using namespace std;

int main(int argc, char** arg) {

	ostringstream ss;
	Session* session;
	Moteur2Interface* m2i;
	HTML* page = new HTML("HomeInCube : Liste des pieces");
	REQUETE* requete = new REQUETE();
	int i, nbPieces;

	if(requete->existGetVar("session")) {

		session = new Session(requete->getGetVar("session"));

	} else {

		session = new Session();

	}

	m2i = session->getMoteur2Interface();

	// On se connecte
	if(!m2i->connect("127.0.0.1", 9000)) {

		cout << "Content-Type: text/html;" << endl << endl
			<< "erreur serveur indisponible" << endl;
		exit(EXIT_SUCCESS);

	}

	page->addCss("#accueil { background:url('images/domotique.png') no-repeat; "
					"height: 600px; "
					"margin-bottom: -300px; "
					"margin-left: auto; "
					"margin-right: auto;"
					"margin-top: -15px; "
					"text-indent: -9999px; }");

	page->addHtmlContent("<div data-role=\"page\" data-theme=\"b\">"
			"<div data-role=\"header\">"
			"<h1>Home In Cube</h1>"
			"<a data-ajax=\"false\" href=\"actualise.cgi\">actualise</a>"
			"</div>"
			"<div data-role=\"content\">"
			"<div id=\"accueil\">logo</div>"
			"<ul data-role=\"listview\" data-inset=\"true\" data-dividertheme=\"b\">"
			"<li data-role=\"list-divider\">Liste des pièces</li>");

	// On récupere la liste des pieces
	nbPieces = m2i->getNbPieces();

	for(i=0; i < nbPieces; i++) {

		ss << "<li>"
			<< "<h3>"
			<< "<a data-ajax=\"false\" href=\"modules.cgi?session=" << session->getId() << "&piece=" <<  i << "\">"
			<< m2i->getNomPiece(i)
			<< "</a>"
			<< "</h3>"
			<< "</li>";

	}

	page->addHtmlContent(ss.str());

	page->addHtmlContent("</ul>"
							"</div> <!-- content -->"
							"<div data-role=\"footer\" data-position=\"fixed\">"
							"Fin de liste"
							"</div>"
							"</div> <!-- page -->");

	page->output();

	session->~Session();

	return 0;

}
