/*
 * html.cpp
 *
 *  Created on: 7 janv. 2012
 *      Author: foxnounours
 */

#include "HTML.hpp"
#include <iostream>

using namespace std;

/**
 * Constructeur qui initialise le contenu de la page
 */
HTML::HTML(string titre) {
	css = "";
	content = "";
	this->titre = titre;
}

/**
 * Methode qui retourne le texte avec les caractères spéciaux replacés par les entités html correspondantes
 */
string HTML::encode(string txt) {
	unsigned int i;
	string s = "";

	for(i = 0; i < txt.size(); i++) {

		switch(txt[i]) {

		case '"' :
			s += "&quot;";
			break;

		case '&' :
			s += "&amp;";
			break;

		case '\'' :
			s += "&#39;";
			break;

		case '<' :
			s += "&lt;";
			break;

		case '>' :
			s += "&gt;";
			break;

		case '^' :
			s += "&circ;";
			break;

/*		case '‘' :
			s += "&lsquo;";
			break;

		case '’' :
			s += "&rsquo;";
			break;

		case '“' :
			s += "&ldquo;";
			break;

		case '”' :
			s += "&rdquo;";
			break;

		case '•' :
			s += "&bull;";
			break;

		case '–' :
			s += "&ndash;";
			break;

		case '—' :
			s += "&mdash;";
			break;

		case '˜' :
			s += "&tilde;";
			break;

		case '™' :
			s += "&trade;";
			break;

		case 'š' :
			s += "&scaron;";
			break;

		case '›' :
			s += "&rsaquo;";
			break;

		case 'œ' :
			s += "&oelig;";
			break;

		case '' :
			s += "&#357;";
			break;

		case 'ž' :
			s += "&#382;";
			break;

		case 'Ÿ' :
			s += "&Yuml;";
			break;

		case '¡' :
			s += "&iexcl;";
			break;

		case '¢' :
			s += "&cent;";
			break;

		case '£' :
			s += "&pound;";
			break;

		case '¥' :
			s += "&yen;";
			break;

		case '¦' :
			s += "&brvbar;";
			break;

		case '§' :
			s += "&sect;";
			break;

		case '¨' :
			s += "&uml;";
			break;

		case '©' :
			s += "&copy;";
			break;

		case 'ª' :
			s += "&ordf;";
			break;

		case '«' :
			s += "&laquo;";
			break;

		case '¬' :
			s += "&not;";
			break;

		case '­' :
			s += "&shy;";
			break;

		case '®' :
			s += "&reg;";
			break;

		case '¯' :
			s += "&macr;";
			break;

		case '°' :
			s += "&deg;";
			break;

		case '±' :
			s += "&plusmn;";
			break;

		case '²' :
			s += "&sup2;";
			break;

		case '³' :
			s += "&sup3;";
			break;

		case '´' :
			s += "&acute;";
			break;

		case 'µ' :
			s += "&micro;";
			break;

		case '¶' :
			s += "&para";
			break;

		case '·' :
			s += "&middot;";
			break;

		case '¸' :
			s += "&cedil;";
			break;

		case '¹/' :
			s += "&sup1;";
			break;

		case 'º' :
			s += "&ordm;";
			break;

		case '»' :
			s += "&raquo;";
			break;

		case '/¼' :
			s += "&frac14;";
			break;

		case '½' :
			s += "&frac12;";
			break;

		case '¾' :
			s += "&frac34;";
			break;

		case '¿' :
			s += "&iquest;";
			break;

		case 'À' :
			s += "&Agrave;";
			break;

		case 'Á' :
			s += "&Aacute;";
			break;

		case 'Â' :
			s += "&Acirc;";
			break;

		case 'Ã' :
			s += "&Atilde;";
			break;

		case 'Ä' :
			s += "&Auml;";
			break;

		case 'Å' :
			s += "&Aring;";
			break;

		case 'Æ' :
			s += "&AElig;";
			break;

		case 'Ç' :
			s += "&Ccedil;";
			break;

		case 'È' :
			s += "&Egrave;";
			break;

		case 'É' :
			s += "&Eacute;";
			break;

		case 'Ê' :
			s += "&Ecirc;";
			break;

		case 'Ë' :
			s += "&Euml;";
			break;

		case 'Ì' :
			s += "&Igrave;";
			break;

		case 'Í' :
			s += "&Iacute;";
			break;

		case 'Î' :
			s += "&Icirc;";
			break;

		case 'Ï' :
			s += "&Iuml;";
			break;

		case 'Ð' :
			s += "&ETH;";
			break;

		case 'Ñ' :
			s += "&Ntilde;";
			break;

		case 'Ò' :
			s += "&Ograve;";
			break;

		case 'Ó' :
			s += "&Oacute;";
			break;

		case 'Ô' :
			s += "&Ocirc;";
			break;

		case 'Õ' :
			s += "&Otilde;";
			break;

		case 'Ö' :
			s += "&Ouml;";
			break;

		case '×' :
			s += "&times;";
			break;

		case 'Ø' :
			s += "&Oslash;";
			break;

		case 'Ù' :
			s += "&Ugrave;";
			break;

		case 'Ú' :
			s += "&Uacute;";
			break;

		case 'Û' :
			s += "&Ucirc;";
			break;

		case 'Ü' :
			s += "&Uuml;'";
			break;

		case 'Ý' :
			s += "&Yacute;";
			break;

		case 'Þ' :
			s += "&THORN;";
			break;

		case 'ß' :
			s += "&szlig;";
			break;

		case 'à' :
			s += "&aacute;";
			break;

		case 'á' :
			s += "&aacute;";
			break;

		case 'â' :
			s += "&acirc;";
			break;

		case 'ã' :
			s += "&atilde;";
			break;

		case 'ä' :
			s += "&auml;";
			break;

		case 'å' :
			s += "&aring;";
			break;

		case 'æ' :
			s += "&aelig;";
			break;

		case 'ç' :
			s += "&ccedil;";
			break;

		case 'è' :
			s += "&egrave;";
			break;

		case 'é' :
			s += "&eacute;";
			break;

		case 'ê' :
			s += "&ecirc;";
			break;

		case 'ë' :
			s += "&euml;";
			break;

		case 'ì' :
			s += "&igrave;";
			break;

		case 'í' :
			s += "&iacute;";
			break;

		case 'î' :
			s += "&icirc;";
			break;

		case 'ï' :
			s += "&iuml;";
			break;

		case 'ð' :
			s += "&eth;";
			break;

		case 'ñ' :
			s += "&ntilde;";
			break;

		case 'ò' :
			s += "&ograve;";
			break;

		case 'ó' :
			s += "&oacute;";
			break;

		case 'ô' :
			s += "&ocirc;";
			break;

		case 'õ' :
			s += "&otilde;";
			break;

		case 'ö' :
			s += "&ouml;";
			break;

		case '÷' :
			s += "&divide;";
			break;

		case 'ø' :
			s += "&oslash;";
			break;

		case 'ù' :
			s += "&ugrave;";
			break;

		case 'ú' :
			s += "&uacute;";
			break;

		case 'û' :
			s += "&ucirc;";
			break;

		case 'ü' :
			s += "&uuml;";
			break;

		case 'ý' :
			s += "&yacute;";
			break;

		case 'þ' :
			s += "&thorn;";
			break;

		case 'ÿ' :
			s += "&yuml";
			break;
*/
		case '\n' :
			s += "<br>\n";
			break;

		default :
			s.push_back(txt[i]);
			break;
		}

	}

	return s;
}

void HTML::addCss(std::string css) {

	this->css = css;

}

/**
 * Methode qui ajoute le texte a la page sans l'encoder
 */
void HTML::addHtmlContent(string htmlContent) {
	content += htmlContent;
}

/**
 * Methode qui ajoute le texte a la page en l'encodant
 */
void HTML::addTxtContent(string txtContent) {
	content += encode(txtContent);
}

/**
 * Envoie la page dans la sortie standar
 */
void HTML::output() {

	// On écrit l'entete http
	cout << "Content-Type: text/html;" << endl << endl;

	// On écrit le haut de la page
	cout << "<!DOCTYPE html>" << endl
		<< "<html>" << endl
		<< "\t<head>" << endl
		<< "\t\t<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\" />" << endl
		<< "\t\t<title>" << titre << "</title>" << endl
		<< "\t\t<link rel=\"stylesheet\" href=\"jquery.mobile-1.0a3.min.css\" />" << endl
		<< "\t\t<script src=\"jquery-1.5.min.js\"></script>" << endl
		<< "\t\t<script src=\"jquery.mobile-1.0a3.min.js\"></script>" << endl;

	if(css.size() > 0) {

		cout << "\t\t<style>" << endl
			<< css << endl
			<< "\t\t</style>" << endl;

	}

	cout << "\t</head>" << endl
		<< "\t<body>" << endl;

	// On écrit le contenu de la page
	cout << content << endl;

	// On écrit le bas de la page
	cout << "\t</body>" << endl
			<< "</html>" << endl;

}

