/*
 * REQUETE.cpp
 *
 *  Created on: 13 janv. 2012
 *      Author: foxnounours
 */

#include "REQUETE.hpp"
#include <cstdlib>
#include <stdio.h>
#include <iostream>

using namespace std;

REQUETE::REQUETE() {

	unsigned int i;
	char *tempChar;
	char tempC;
	string temp, temp2, key="", value="";

	// On parse le get
	tempChar = getenv("QUERY_STRING");

	if(tempChar != NULL) {

		temp = tempChar;

		if(temp.size() > 0) {

			for(i=0; i<temp.size(); i++) {

				if(temp[i] == '=') {

					key = temp2;
					temp2 = "";

				} else if(temp[i] == '&') {

					value = temp2;
					temp2 = "";
					getVars.insert(pair<string, string>(key, value));

				} else {

					temp2 += temp[i];

				}

			}

			getVars.insert(pair<string, string>(key, temp2));

		}

	}

	// On parse le post
	if(cin) {
		temp = "";
		temp2 = "";
		cin >> temp;
		for(i=0; i<temp.size(); i++) {

			if(temp[i] == '=') {

				key = temp2;
				temp2 = "";

			} else if(temp[i] == '&') {

				value = temp2;
				temp2 = "";
				postVars.insert(pair<string, string>(key, value));

			} else {

				temp2 += temp[i];

			}

		}

		postVars.insert(pair<string, string>(key, temp2));

	}

}

bool REQUETE::existGetVar(std::string key) {

	if(getVars.count(key))
		return true;

	return false;

}

std::string REQUETE::getGetVar(std::string key) {

	return getVars[key];

}

bool REQUETE::existPostVar(std::string key) {

	if(postVars.count(key))
		return true;

	return false;

}

std::string REQUETE::getPost(std::string key) {

	return postVars[key];

}
