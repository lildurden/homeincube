#ifndef _LUMIERE_HPP
#define _LUMIERE_HPP
#define TYPE_LUMIERE 2

#include <string>
#include <fstream>
#include "Module.hpp"

class Lumiere : public Module {
private:
	int dim;
	int bright;

public :
	Lumiere (int, std::string, int, bool, int,int);
	int getDim();
	int getBright();
	void setDim(int);
	void setBright(int);
	void setBrightDim(int bright, int dim);
	void setAll(bool, int, int);
	Lumiere & operator=(const Lumiere & source);
	virtual int getType();
	virtual std::string toString();
	virtual ~Lumiere();

	virtual std::string serialize();

	static Lumiere* unserialize(std::ifstream&);
};

#endif
