#include "Prise.hpp"

#include <sstream>

using namespace std;

Prise::Prise(int idC , string nom, int idPieceC , bool etatC)
:Module(idC,nom,idPieceC,etatC){

}

int Prise::getType() {
	return TYPE_PRISE;
}

string Prise::toString() {
	ostringstream s;
	s << "Prise " << getId() << " : "
			<< getNom() <<  " est dans la piece "
			<< getIdPiece() << " et sont état est "
			<< getEtat() << endl;
	return s.str();
}

Prise::~Prise() {

}

std::string Prise::serialize() {

	ostringstream ss;

	ss << getType() << endl
		<< getId() << endl
		<< getNom() << endl
		<< getIdPiece() << endl
		<< getEtat() << endl;

	return ss.str();

}

Prise* Prise::unserialize(std::ifstream& file) {

	bool etat;
	unsigned int id, idPiece;
	string nom;

	file >> id;
	getline(file,nom);
	getline(file,nom);
	file >> idPiece
		>> etat;

	return new Prise(id, nom, idPiece, etat);

}
