/*
 * User.hpp
 *
 *  Created on: 5 janv. 2012
 *      Author: foxnounours
 */

#ifndef _USER_HPP
#define _USER_HPP

#include <string>
#include <fstream>

class User {

private:

	std::string nom;
	std::string mdp;

public:

	User(std::string, std::string);
    std::string getNom();
    std::string getMdp();
    bool connect();
	static User* unserialize(std::ifstream&);
	std::string serialize();

};

#endif /* USER_HPP_ */
