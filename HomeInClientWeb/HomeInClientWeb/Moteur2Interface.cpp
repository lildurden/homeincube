/*
 * Moteur2Interface.cpp
 *
 *  Created on: 5 janv. 2012
 *      Author: foxnounours
 */

#include "Moteur2Interface.hpp"
#include <sstream>
#include <iostream>
#include "ListPieces.hpp"
#include "Piece.hpp"
#include "Module.hpp"
#include "Lumiere.hpp"
#include "Volet.hpp"

using namespace std;


Moteur2Interface::Moteur2Interface() {

}

Moteur2Interface::Moteur2Interface(Connection* connection, User* user) {

	this->connection = connection;
	//this->user = user;

}

Moteur2Interface* Moteur2Interface::unserialize(ifstream& file) {

	Connection* connectionTemp;
	User* userTemp;

	connectionTemp = Connection::unserialize(file);

	//userTemp = User::unserialize(file);

	ListPieces::unserialize(file);

	return new Moteur2Interface(connectionTemp, userTemp);

}

string Moteur2Interface::serialize() {

	ostringstream ss;

	ss << connection->serialize()
//			<< user->serialize()
			<< ListPieces::get()->serialize();

	return ss.str();

}

/**
 * Connecte l'utilisateur et retourne reussie
 */
bool Moteur2Interface::connect(const char* adresse, int port) {

	connection = new Connection(adresse, port);
	return connection->connect();

}

/**
 * Connecte l'utilisateur et retourne reussie
 */
bool Moteur2Interface::connectUser(const char* nom, const char* mdp) {

	user = new User(nom, mdp);
	return user->connect();

}

/**
 * Retourne le nombre de pieces
 */
int Moteur2Interface::getNbPieces() {

	return ListPieces::get()->getNbPiece();

}

/**
 * Retourne l'id de la piece
 */
int Moteur2Interface::getIdPiece(int indexPiece) {

	return ListPieces::get()->getByIndex(indexPiece)->getIdPiece();

}

/**
 * Retourne le nom de la piece
 */
const char* Moteur2Interface::getNomPiece(int indexPiece) {

	return ListPieces::get()->getByIndex(indexPiece)->getNom().c_str();

}

/**
 * Retourne le nombre de module de la piece
 */
int Moteur2Interface::getNbModule(int indexPiece) {

	return ListPieces::get()->getByIndex(indexPiece)->getNbModule();

}

/**
 * Retourne l'id du module
 */
int Moteur2Interface::getIdModule(int indexPiece, int indexModule) {

	return ListPieces::get()->getByIndex(indexPiece)->getModuleByIndex(indexModule)->getId();

}

/**
 * Retourne le nom du module
 */
const char* Moteur2Interface::getNomModule(int indexPiece, int indexModule) {

	return ListPieces::get()->getByIndex(indexPiece)->getModuleByIndex(indexModule)->getNom().c_str();

}

/**
 * Retourne le type du module
 */
int Moteur2Interface::getTypeModule(int indexPiece, int indexModule) {

	return ListPieces::get()->getByIndex(indexPiece)->getModuleByIndex(indexModule)->getType();

}

/**
 * Retourne l'etat du module
 */
bool Moteur2Interface::getEtatModule(int indexPiece, int indexModule) {

	return ListPieces::get()->getByIndex(indexPiece)->getModuleByIndex(indexModule)->getEtat();

}

/**
 * Modifie l'etat du module
 */
void Moteur2Interface::setEtatModule(int indexPiece, int indexModule, bool etat) {

	ListPieces::get()->getByIndex(indexPiece)->getModuleByIndex(indexModule)->setEtat(etat);

}

/**
 * Retourne le bright de la lumiere
 */
int Moteur2Interface::getBrightLumiere(int indexPiece, int indexLumiere) {

	return ((Lumiere*)ListPieces::get()->getByIndex(indexPiece)->getModuleByIndex(indexLumiere))->getBright();

}

/**
 * Modifie le bright de la lumiere
 */
void Moteur2Interface::setBrightLumiere(int indexPiece, int indexLumiere, int bright) {

	((Lumiere*)ListPieces::get()->getByIndex(indexPiece)->getModuleByIndex(indexLumiere))->setBright(bright);

}

/**
 * Retourne le dim de la lumiere
 */
int Moteur2Interface::getDimLumiere(int indexPiece, int indexLumiere) {

	return ((Lumiere*)ListPieces::get()->getByIndex(indexPiece)->getModuleByIndex(indexLumiere))->getDim();

}

/**
 * Modifie le dim de la lumiere
 */
void Moteur2Interface::setDimLumiere(int indexPiece, int indexLumiere, int dim) {

	((Lumiere*)ListPieces::get()->getByIndex(indexPiece)->getModuleByIndex(indexLumiere))->setDim(dim);

}

void Moteur2Interface::setBrightDimLumiere(int indexPiece, int indexLumiere, int bright, int dim) {

	((Lumiere*)ListPieces::get()->getByIndex(indexPiece)->getModuleByIndex(indexLumiere))->setBrightDim(bright, dim);

}

void Moteur2Interface::setLumiere(int indexPiece, int indexLumiere, bool etat, int bright, int dim) {

	((Lumiere*)ListPieces::get()->getByIndex(indexPiece)->getModuleByIndex(indexLumiere))->setAll(etat, bright, dim);

}

/**
 * Retourne la progression du volet
 */
int Moteur2Interface::getProgressionVolet(int indexPiece, int indexVolet) {

	return ((Volet*)ListPieces::get()->getByIndex(indexPiece)->getModuleByIndex(indexVolet))->getProgression();

}

/**
 * Modifie la progression du volet
 */
void Moteur2Interface::setProgressionVolet(int indexPiece, int indexVolet, int progression) {

	((Volet*)ListPieces::get()->getByIndex(indexPiece)->getModuleByIndex(indexVolet))->setProgression(progression);

}

/**
 * Modifie le volet
 */
void Moteur2Interface::setVolet(int indexPiece, int indexVolet, bool etat, int progression) {

	((Volet*)ListPieces::get()->getByIndex(indexPiece)->getModuleByIndex(indexVolet))->setAll(etat, progression);

}
