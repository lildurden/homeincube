/*
 * modules.cpp
 *
 *  Created on: 13 janv. 2012
 *      Author: foxnounours
 */
#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include "Session.hpp"
#include "Moteur2Interface.hpp"
#include "HTML.hpp"
#include "REQUETE.hpp"

using namespace std;

int main(int argc, char** arg) {

	ostringstream outs;
	Session* session;
	Moteur2Interface* m2i;
	HTML* page = new HTML("HomeInCube : Controler un module");
	REQUETE* requete = new REQUETE();
	int idModule, idPiece;

	if(requete->existGetVar("session")) {

		session = new Session(requete->getGetVar("session"));

	} else {

		session = new Session();

	}

	if(requete->existGetVar("piece")) {

		idPiece = atoi(requete->getGetVar("piece").c_str());

	} else {

		cout << "Location: pieces.cgi?session=" << session->getId() << endl << endl;
		exit(0);

	}

	if(requete->existGetVar("module")) {

		idModule = atoi(requete->getGetVar("module").c_str());

	} else {

		cout << "Location: modules.cgi?session=" << session->getId()
				<< "&piece=" << idPiece << endl << endl;
		exit(0);

	}

	m2i = session->getMoteur2Interface();

	// On se connecte
	if(!m2i->connect("127.0.0.1", 9000)) {

		cout << "Content-Type: text/html;" << endl << endl
			<< "erreur serveur indisponible" << endl;
		exit(EXIT_SUCCESS);

	}

	outs << "<div data-role=\"page\" data-theme=\"b\">"
			"<div data-role=\"header\">"
			"<a data-ajax=\"false\" href=\"modules.cgi?session=" << session->getId() << "&piece=" <<  idPiece << "\">retour</a>"
			"<h1>Home In Cube</h1>"
			"<a data-ajax=\"false\" href=\"actualise.cgi?piece=" << idPiece << "&module=" << idModule << "\">actualise</a>"
			"</div>"
			"<div data-role=\"content\">"
			"<h2>"
		<< m2i->getNomModule(idPiece, idModule)
		<< " / "
		<< m2i->getNomPiece(idPiece)
		<< "</h2>"
			"<form data-ajax=\"false\" action=\"commander.cgi?session=" << session->getId()
										<< "&piece=" << idPiece <<
										"&module=" << idModule << "\" method=\"post\">"
				"<div data-role=\"fieldcontain\">"
					"<label for=\"etat\">Interrupteur:</label>"
					"<select name=\"etat\" id=\"etat\" data-role=\"slider\">"
						"<option value=\"0\""
		<<					(m2i->getEtatModule(idPiece, idModule) ? "" : "selected=\"selected\"")
		<<					">Off</option>"
						"<option value=\"1\""
		<<					(m2i->getEtatModule(idPiece, idModule) ? "selected=\"selected\"" : "")
		<<					">On</option>"
					"</select>"
				"</div>";

	switch(m2i->getTypeModule(idPiece, idModule)) {

	case 2 :

		outs << "<div data-role=\"fieldcontain\">"
					"<label for=\"bright\">Luminosit&eacute;:</label>"
					"<input type=\"range\" name=\"bright\" id=\"bright\" value=\""
			<<			m2i->getBrightLumiere(idPiece, idModule)
			<<			"\" min=\"0\" max=\"255\" />"
				"</div>"
				"<div data-role=\"fieldcontain\">"
					"<label for=\"dim\">Vitesse de transition:</label>"
					"<input type=\"range\" name=\"dim\" id=\"dim\" value=\""
			<<			m2i->getDimLumiere(idPiece, idModule)
			<<			"\" min=\"0\" max=\"100\" />"
				"</div>";

		break;

	case 3 :

		outs <<"<div data-role=\"fieldcontain\">"
					"<label for=\"progression\">Progression:</label>"
					"<input type=\"range\" name=\"progression\" id=\"progression\" value=\""
			<<			m2i->getProgressionVolet(idPiece, idModule)
			<<			"\" min=\"0\" max=\"100\" />"
				"</div>";

		break;

	}

	outs <<"<div data-role=\"fieldcontain\">"
				"<input type=\"submit\"value=\"Valider\" />"
			"</div>";

	page->addHtmlContent(outs.str());

	page->addHtmlContent("</form>");

	page->addHtmlContent("</div> <!-- content -->"
						"<div data-role=\"footer\" data-position=\"fixed\">"
						"Fin de liste"
						"</div>"
						"</div> <!-- page -->");

	page->output();

	session->~Session();

	return 0;

}
