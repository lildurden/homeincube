/*
 * User.cpp
 *
 *  Created on: 5 janv. 2012
 *      Author: foxnounours
 */

#include "User.hpp"
#include <sstream>
#include "Parser.hpp"

using namespace std;

User::User(std::string nom, std::string mdp) {

	this->nom = nom;
	this->mdp = mdp;

}

string User::getNom()
{
    return nom;
}

string User::getMdp()
{
    return mdp;
}

bool User::connect() {

	return Parser::get()->connect(this);

}

User* User::unserialize(ifstream& file) {

	string nom, mdp;

	file >> nom;
	file >> mdp;

	return new User(nom, mdp);

}

std::string User::serialize() {

	ostringstream ss;

	ss << nom << endl
		<< mdp << endl;

	return ss.str();

}

