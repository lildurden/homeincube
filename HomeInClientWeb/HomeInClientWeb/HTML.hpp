/*
 * html.hpp
 *
 *  Created on: 7 janv. 2012
 *      Author: foxnounours
 */

#ifndef _HTML_HPP
#define _HTML_HPP

#include <string>

class HTML {

private :

	std::string css;
	std::string content;
	std::string titre;
	std::string encode(std::string txt);

public :

	HTML(std::string titre);
	void addCss(std::string);
	void addHtmlContent(std::string htmlContent);
	void addTxtContent(std::string txtContent);
	void output();

};

#endif /* HTML_HPP_ */
