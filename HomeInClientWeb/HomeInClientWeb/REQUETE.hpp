/*
 * REQUETE.hpp
 *
 *  Created on: 13 janv. 2012
 *      Author: foxnounours
 */

#ifndef REQUETE_HPP_
#define REQUETE_HPP_

#include <string>
#include <map>

class REQUETE {

private:

	std::map<std::string, std::string> getVars;
	std::map<std::string, std::string> postVars;

public:

	REQUETE();
	bool existGetVar(std::string);
	std::string getGetVar(std::string);
	bool existPostVar(std::string);
	std::string getPost(std::string);

};

#endif /* REQUETE_HPP_ */
