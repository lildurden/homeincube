// Protection contre la redefinition de la classe
#ifndef _PARSER_HPP
#define _PARSER_HPP

// On inclue les librairies et les headers necessaires
#include <string>
#include <vector>
#include "User.hpp"
#include "Piece.hpp"
#include "Module.hpp"
#include "Prise.hpp"
#include "Lumiere.hpp"
#include "Volet.hpp"

/**
 * Classe qui à la gestion des trames.
 * Elle s'occupe de découper les trames reçues et d'appeler la méthode adéquoite
 * avec les arguments adéquoits. Elle s'occupe également de générer les trames qui devront être envoyées.
 * Afin que toutes les classes puissent appeler le parser pour demander ou envoyer des informations
 * au server, cette classes est une classe singleton.
 */
class Parser {

private:

	/**
	 * Cette attribut static est à vrai si il existe déjà une instance de cette classe.
	 */
	static bool instancied;

	/**
	 * Cette attribut static stocke l'unique instance de la classe Parser.
	 */
	static Parser* parser;

	/**
	 * Cette methode découpe les arguments d'une trame et les retournes sous forme de tableau.
	 * A la fin de l'execution de la methode tSize pointe vers un entier représentant le nombre d'argument
	 * de la trame.
	 */
	std::string* split(std::string str2split, int *tSize);

public:

	/**
	 * Methode qui retourne l'unique instance de la classe Parser.
	 */
	static Parser* get();

	/**
	 * Methode qui connect un utilisateur et retourne vrai si reussi
	 */
	bool connect(User* user);

	/**
	 * Methode qui retourne un vector qui contient l'ensemble des pieces
	 */
	std::vector<Piece*> getPieces();

	/**
	 * Methode qui retourne un vector qui contient l'ensemble des modules d'une pieces
	 */
	std::vector<Module*> getModules(int idPiece);

	/**
	 * Methode qui envoie une trame au serveur pour que le module réel
	 * possède les même propriétés que celui en parametre.
	 */
	void setModule(Module* module);

	/**
	 * Methode qui envoie une trame au serveur pour que la prise réel
	 * possède les même propriétés que celle en parametre.
	 */
	void setModule(Prise* prise);

	/**
	 * Methode qui envoie une trame au serveur pour que la lumiere réel
	 * possède les même propriétés que celle en parametre.
	 */
	void setModule(Lumiere* lumiere);

	/**
	 * Methode qui envoie une trame au serveur pour que le volet réel
	 * possède les même propriétés que celui en parametre.
	 */
	void setModule(Volet* volet);

};

#endif
