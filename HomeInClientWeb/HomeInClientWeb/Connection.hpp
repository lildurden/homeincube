/*
 * connection.hpp
 *
 *  Created on: 5 janv. 2012
 *      Author: foxnounours
 */

#ifndef _CONNECTION_HPP
#define _CONNECTION_HPP

#include <string>
#include <fstream>

class Connection {

private:

	std::string adresse;
	int port;

public:

	Connection(std::string, int);
	bool connect();
	static Connection* unserialize(std::ifstream&);
	std::string serialize();

};

#endif /* _CONNECTION_HPP */
