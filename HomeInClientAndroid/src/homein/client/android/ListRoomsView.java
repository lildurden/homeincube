package homein.client.android;

import java.io.Serializable;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ListView;

import android.view.View.OnClickListener;
import android.app.Activity;
import java.lang.System;
import android.os.Bundle;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.SeekBar;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

import android.graphics.Color;

public class ListRoomsView extends BaseAdapter implements Serializable{
   
    private static final long serialVersionUID = 1L;

    private Context ctx;
    private LayoutInflater mInflater;
    private final int rowLayout=R.layout.list_row_pieces;
    private String[] theName;
    private int count;
    private static ListView lv;
    private Boolean[] theCheck;
    private View trucView;
   
    private String [] LesPieces;
    private boolean empty= true;
    
    private int selectedPos = -1;


    public ListRoomsView(Context ictx,String [] itheName,ListView lv){
	ctx=ictx;
	count=itheName.length;
	mInflater= LayoutInflater.from(ctx);
	theName=new String[count];
	theName=itheName;
	this.lv=lv;
    }
    public void setEmpty(boolean truc){
	empty=truc;
    }
    public boolean empty(){
	return empty;
    }
    public int getCount(){
	return count;
    }
    
    public Object getItem(int position){
	return null;
    }
    
    public long getItemId(int position){
	return 0;
    }
    
    public View getView(int position, View convertView, ViewGroup parent){
	if(convertView==null){
	    convertView=mInflater.inflate(rowLayout,null);
	}
	trucView=convertView;
	TextView tv=(TextView) convertView.findViewById(R.id.thetext);
	tv.setText(theName[position]);
	convertView.setTag(new Long(position));
	
	if(selectedPos == position){
	    tv.setBackgroundColor(Color.TRANSPARENT);
	}else{
	    tv.setBackgroundColor(Color.WHITE);
	}

	
	return convertView;
    }

    public void setSelectedPosition(int pos){
	selectedPos = pos;
	notifyDataSetChanged();
    }

    public int getSelectedPosition(){
	return selectedPos;
    }

}
