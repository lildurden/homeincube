package homein.client.android;
import java.io.Serializable;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ListView;

import android.view.View.OnClickListener;
import android.app.Activity;
import java.lang.System;
import android.os.Bundle;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.SeekBar;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

import android.graphics.Color;

public class ListModsView extends BaseAdapter implements Serializable{
    private Context ctx;
    private LayoutInflater mInflater;
    private final int rowLayout=R.layout.list_row_mods;
    private String[] theName;
    private int count;
    private static ListView lv;
    private boolean[] etat;
    private View trucView;
    private TextView tv;
    private String [] LesPieces;

    
    private int selectedPos = -1;


    public ListModsView(Context ictx,String [] itheName,boolean [] state,ListView lv){
	ctx=ictx;
	count=itheName.length;
	etat=new boolean[state.length];
	etat=state;
	mInflater= LayoutInflater.from(ctx);
	theName=new String[count];
	theName=itheName;
	this.lv=lv;
    }
    public int getCount(){
	return count;
    }
    
    public Object getItem(int position){
	return null;
    }
    
    public long getItemId(int position){
	return 0;
    }
    public void setEtat(int pos,boolean etatP){
	selectedPos = -1;
	etat[pos]=etatP;
	// inform the view of this change
	notifyDataSetChanged();
    }
    public View getView(int position, View convertView, ViewGroup parent){
	if(convertView==null){
	    convertView=mInflater.inflate(rowLayout,null);
	}
	trucView=convertView;
	tv=(TextView) convertView.findViewById(R.id.textmod);
	tv.setText(theName[position]);
	convertView.setTag(new Long(position));
	if(etat[position]==true){
	    trucView.setBackgroundColor(Color.WHITE);
	    convertView.setAlpha((float)0.8);
	}
	else{
	    trucView.setBackgroundColor(Color.GRAY);
	    trucView.setAlpha((float)0.8);
	}
	if(selectedPos == position){
	    tv.setBackgroundColor(Color.TRANSPARENT);
	    convertView.setAlpha((float)0.2);  
	}
	else{
	    tv.setAlpha((float)1);
	    convertView.setAlpha((float)1);
	}	
	return convertView;
    }

    public void setSelectedPosition(int pos){
	selectedPos = pos;
	notifyDataSetChanged();
    }

    public int getSelectedPosition(){
	return selectedPos;
    }

}
