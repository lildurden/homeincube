package homein.client.android;

import android.app.Activity;
import android.content.Context;

import android.os.Bundle;
import android.util.Log;

import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView;
import android.view.View.OnClickListener;
import android.view.View;
import android.widget.TextView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import android.database.sqlite.SQLiteDatabase;
import android.database.Cursor;

public class RoomsView extends Activity{

    private DataBase db;
    private SQLiteDatabase bdd;

    private static final String TAG = "RoomsView";
    
    private Context ctx;
    private ListRoomsView customRooms;    
    private ListModsView customMods;
    private static ListView lv;
    //private static ListView lv2;
    private static ListView lv3;

    private MotorPeer Motor;
    private int PositionPiece;
    private int PositionMod;

    private String [] LesPieces;
    private String [] NomMods;
    private int []IdRooms;
    private boolean [] EtatMods;
    private int [] Dim;
    private int [] Bright;
    private int [] Progression;
    private int [] TypeMods;

    private boolean saved=false;

    private TextView tv;
    private TextView tvDim;
    private TextView tvBright;

    private SeekBar SeekDim;
    private SeekBar SeekBright;
    

    private static final String KEYLISTROOM="ListRoom";
    private static final String KEYLISTMODS="ListMods";
    private static final String KEYMOTOR="Motor";
    private static final String KEYPOSMOD="PosMod";
    private static final String KEYPOSPIECE="PosPiece";

    /** Called when the activity is first created. */
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.personnal_list_view);
	if(savedInstanceState!=null) {
	    customRooms  = ( ListRoomsView) savedInstanceState.getSerializable(KEYLISTROOM);
	    PositionMod  =  savedInstanceState.getInt(KEYPOSMOD);
	    PositionPiece  =  savedInstanceState.getInt(KEYPOSPIECE);
	    Motor=(MotorPeer)savedInstanceState.getSerializable(KEYMOTOR);
	    customMods = (ListModsView) savedInstanceState.getSerializable(KEYLISTMODS);
	    saved=true;
	    Log.d(TAG, "saved()");
	}
	else{
	    Bundle extras = getIntent().getExtras();
	    String Addr=(String)extras.get("Addr");
	    String Port=(String)extras.get("Port");

	    Motor=new MotorPeer(ctx,Addr,Port);
	}
	ctx=this;

	fillData();
	tv=(TextView) findViewById(R.id.nom_module);
	tvDim=(TextView) findViewById(R.id.text_dim);
	tvBright=(TextView) findViewById(R.id.text_bright);
	tvDim.setText("");
	tvBright.setText("");
	SeekDim=(SeekBar) findViewById(R.id.seekdim);
	SeekBright=(SeekBar) findViewById(R.id.seekbright);
	SeekDim.setVisibility(4);
	SeekBright.setVisibility(4);
	Log.d(TAG, "onCreate()");

    }
    public void onSaveInstanceState(Bundle savedInstanceState) {
	customRooms.setEmpty(false);
	savedInstanceState.putSerializable(KEYMOTOR, Motor);
	savedInstanceState.putInt(KEYPOSPIECE, PositionPiece);
	savedInstanceState.putInt(KEYPOSMOD, PositionMod);

	savedInstanceState.putSerializable(KEYLISTMODS, customMods);
	savedInstanceState.putSerializable(KEYLISTROOM,customRooms);
	super.onSaveInstanceState(savedInstanceState);
    }
    public void onRestoreInstanceState(Bundle savedInstanceState) {
	super.onRestoreInstanceState(savedInstanceState);
	Motor=(MotorPeer)savedInstanceState.getSerializable(KEYMOTOR);
	customRooms  = ( ListRoomsView) savedInstanceState.getSerializable(KEYLISTROOM);
	customMods = (ListModsView) savedInstanceState.getSerializable(KEYLISTMODS);
	saved=true;

    }

    private void fillData(){
	
	if(!saved){
	    Motor.AskForRooms();
	}

	int nb=Motor.getNbRooms();

	String [] nameR=new String[nb];
	int [] idR=new int[nb];

	idR=Motor.getIdRooms();
	nameR=Motor.getNameRooms();
	

	lv=(ListView) findViewById(R.id.clv);
	lv.setLongClickable (true);
	Log.d(TAG, "onCreate()");

	if(!saved){
	    customRooms=new ListRoomsView(ctx,Motor.getNameRooms(),lv);
	}
	lv.setAdapter(customRooms);
	if(!customRooms.empty()){
	    SetListMods(0);
	}
	lv.setOnItemClickListener(new OnItemClickListener() {
		public void onItemClick(AdapterView<?> a, 
					View v, int position, long id) {
		    PositionPiece=position;
		    Motor.AskForMods(position);
		    customRooms.setSelectedPosition(position);
		    SetListMods(position);
		    SeekDim.setVisibility(4);
		    SeekBright.setVisibility(4);
		    tvDim.setText("");
		    tvBright.setText("");
		    tv.setText("");

		}
	    });   
    }

    public void SetListMods(int position){
	
	ListView lv2=(ListView) findViewById(R.id.mods);

	Dim=Motor.getDimMods();
	Bright=Motor.getBrightMods();
	Progression=Motor.getProgressionMods();
	TypeMods=Motor.getTypeMods();
	NomMods=Motor.getNameMods();
	EtatMods=Motor.getEtatMods();
	if(customRooms.empty()){
	    customMods=new ListModsView(ctx,NomMods,EtatMods,lv);
	}
	
	lv2.setAdapter(customMods);
	lv2.setOnItemClickListener(new OnItemClickListener() {
		public void onItemClick(AdapterView<?> a,View v, 
					int position, long id) {
		    EtatMods[position]=!EtatMods[position];
		    customMods.setEtat(position,EtatMods[position]);
		    Motor.setEtatMod(PositionPiece,position,EtatMods[position]);
		    
		    SeekDim.setVisibility(4);
		    SeekBright.setVisibility(4);
		    tvDim.setText("");
		    tvBright.setText("");
		    tv.setText(NomMods[position]);
		}
	    });
		    
	lv2.setOnItemLongClickListener(new OnItemLongClickListener() {
		public boolean onItemLongClick(AdapterView<?> a,View v,int position, long id) {
		    customMods.setSelectedPosition(position);
		    PositionMod=position;
		    switch(TypeMods[position]){
		    case 0:
			SeekDim.setVisibility(4);
			SeekBright.setVisibility(4);
			tvDim.setText("");
			tvBright.setText("");
			break;
		    case 1:
			SeekDim.setVisibility(4);
			SeekBright.setVisibility(4);
			tvDim.setText("");
			tvBright.setText("");
			break;
		    case 2:
			SeekDim.setVisibility(0);
			SeekBright.setVisibility(0);
			SeekDim.setProgress(Dim[position]);
			SeekBright.setProgress(Bright[position]);
			tvDim.setText("Dim");
			tvBright.setText("Bright");
			break;
		    case 3:
			SeekDim.setVisibility(0);
			SeekBright.setVisibility(4);
			tvDim.setText("Progression");
			tvBright.setText("");
			SeekDim.setProgress(Progression[position]);

			break;
		    default:
		    
			break;
		    }
		    tv.setText(NomMods[position]);
		    progressBar(position);
		    return true;
		}
	    });
	customRooms.setEmpty(true);
    }
    public void progressBar(int position){
	SeekDim.setOnSeekBarChangeListener (new OnSeekBarChangeListener (){
		public void onStopTrackingTouch(SeekBar arg0) {
		    Dim[PositionMod]=arg0.getProgress();
		    if(TypeMods[PositionMod]==2)
			Motor.setDimMod(PositionPiece,PositionMod,Dim[PositionMod]);
		    else
			Motor.setProgressMod(PositionPiece,PositionMod,Dim[PositionMod]);
		    Toast.makeText(ctx," "+arg0.getProgress(), Toast.LENGTH_SHORT).show();
		     
		}
		public void onStartTrackingTouch(SeekBar arg0) {

		}

		public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2){
		    
		}
	    });
	SeekBright.setOnSeekBarChangeListener (new OnSeekBarChangeListener (){
		public void onStopTrackingTouch(SeekBar arg0) {
		    Bright[PositionMod]=arg0.getProgress();
		    Motor.setBrightMod(PositionPiece,PositionMod,Bright[PositionMod]);
		    Toast.makeText(ctx," "+arg0.getProgress(), Toast.LENGTH_SHORT).show();
		
		}
		public void onStartTrackingTouch(SeekBar arg0) {
		}
		public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2){
		    
		}
	    });
    }
    protected void onStart(){
	//Au démarrage de l'application
	
	super.onStart();
	Log.d(TAG, "onStart()");
    }
    
    protected void onRestart(){
	super.onRestart();
	Log.d(TAG, "onRestart()");	
    }
    
    protected void onResume(){
	super.onResume();
	Log.d(TAG, "onResume()");
    }
    
    protected void onPause(){
	super.onPause();
	Log.d(TAG, "onPause()");
	
    }
    
    protected void onStop(){
	super.onStop();
	Log.d(TAG, "onStop()");
    }
    
    protected void onDestroy(){
	super.onDestroy();
	Log.d(TAG, "onDestroy()");
    }
    
}
