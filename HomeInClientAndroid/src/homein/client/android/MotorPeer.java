package homein.client.android;
import java.io.Serializable;

import android.util.Log;
import android.content.Context;
import android.widget.Toast;
import android.os.Bundle;
import android.app.Activity;

public class MotorPeer implements Serializable{
    private static final long serialVersionUID = 1L;
    private static final String TAG = "RoomsView";

    static{
	System.loadLibrary("HomeInMotor");
    }

    private long objPeer;
    protected native long nativ_createPeer();

    public native boolean nativ_Connect(long obj,String AddrIp,int port);
    public native boolean nativ_ConnectUser(long obj,String nom,String mdp);

    public native int nativ_getNbRooms(long obj);
    public native String nativ_getNameRoom(long obj, int indexR);
    public native int nativ_getIdRoom(long obj,int indexR);
    public native int nativ_getNbMods(long obj,int indexR);
    public native int nativ_getIdMod(long obj, int indexR,int indexM);
    public native String nativ_getNomMod(long obj,int indexP,int indexM);
    public native int nativ_getTypeMod(long obj,int indexP,int indexMod);
    public native boolean nativ_getEtatMod(long obj, int indexP,int indexMod);
    public native void nativ_setEtatModule(long obj,int indexP,int indexMod,boolean etat);
    public native int nativ_getBrightLumiere(long obj,int indexPiece, int indexLumiere);
    public native void nativ_setBrightLumiere(long obj,int indexPiece, int indexLumiere, int bright);
    public native int nativ_getDimLumiere(long obj,int indexPiece, int indexLumiere);
    public native void nativ_setDimLumiere(long obj,int indexPiece, int indexLumiere, int dim);
    public native int nativ_getProgressionVolet(long obj,int indexPiece, int indexVolet);
    public native void nativ_setProgressionVolet(long obj,int indexPiece, int indexVolet, int progression);


    private String [] NameRooms;
    private int [] IdRooms;
    private int NbRooms;

    private String [] NameMods;
    private int [] IdMods;
    private int [] TypeMods;
    private boolean [] EtatMods;
    private int [] Dim;
    private int [] Bright;
    private int [] Progression;
    private int NbMods;
    private Context ctx;
    private boolean connect=false;

    public MotorPeer(){
    }

    public MotorPeer(Context cont,String addr,String port){
	ctx=cont;
	objPeer=nativ_createPeer();

	boolean machin=nativ_Connect(objPeer,addr,new Integer(port));

	connect=true;
	Log.d(TAG, "constructeur");

    }
    public MotorPeer(MotorPeer Motor){
	Log.d(TAG, "Constructeur recopie");

	objPeer=Motor.getObjPeer();
	Log.d(TAG, "Constructeur recopie");

	NbRooms=Motor.getNbRooms();
	IdRooms=new int[NbRooms];
	NameRooms=new String[NbRooms];
	IdRooms=Motor.getIdRooms();
	NameRooms=Motor.getNameRooms();
	connect=true;
	Log.d(TAG, "constructeur");
    }
   
    public long getObjPeer(){
	return objPeer;
    }
    public boolean empty(){
	return connect;
    }
    public int getNbRooms(){
	Log.d(TAG, "getNbRooms");

	return NbRooms;
	//Log.d(TAG, "getNbRooms");

    }
    public void AskForRooms(){
	NbRooms=nativ_getNbRooms(objPeer);
	IdRooms=new int[NbRooms];
	NameRooms=new String[NbRooms];
	for(int i=0;i<NbRooms;i++){
	    NameRooms[i]=nativ_getNameRoom(objPeer,i);
	    IdRooms[i]=nativ_getIdRoom(objPeer,i);
	}
	Log.d(TAG, "AskRooms");

    }
    public String [] getNameRooms(){
	return NameRooms;
    }
    public int[] getIdRooms(){
	return IdRooms;
    }

    public void AskForMods(int indexR){
	NbMods=nativ_getNbMods(objPeer,indexR);
	
	NameMods=new String[NbMods];
	IdMods=new int[NbMods];
	TypeMods= new int[NbMods];
	EtatMods=new boolean[NbMods];
	Dim=new int[NbMods];
	Bright=new int[NbMods];
	Progression=new int[NbMods];
	for(int i=0;i<NbMods;i++){
	    IdMods[i]=nativ_getIdMod(objPeer,indexR,i);
	    NameMods[i]=nativ_getNomMod(objPeer,indexR,i);
	    EtatMods[i]=nativ_getEtatMod(objPeer,indexR,i);
	    TypeMods[i]=nativ_getTypeMod(objPeer,indexR,i);
	    switch(TypeMods[i]){
	    case 0:
		break;
	    case 1:
		break;
	    case 2:
		Dim[i]=nativ_getDimLumiere(objPeer,indexR,i);
		Bright[i]=nativ_getBrightLumiere(objPeer,indexR,i);
		break;
	    case 3:
		Progression[i]=nativ_getProgressionVolet(objPeer,indexR,i);
		break;
	    default:
		break;
	    }
	}
	Log.d(TAG, "AskMods");

    }
    public String[] getNameMods(){
	return NameMods;
    }
    public boolean[] getEtatMods(){
	return EtatMods;
    }
    public int[] getIdMods(){
	return IdMods;
    }
    public int[] getTypeMods(){
	return TypeMods;
    }
    public int[] getDimMods(){
	return Dim;
    }
    public int[] getBrightMods(){
	return Bright;
    }
    public int[] getProgressionMods(){
	return Progression;
    }
    public int getIdOneMod(int position){
	return IdMods[position];
    }
    public void setEtatMod(int indexPiece,int indexMod,boolean etat){
	EtatMods[indexMod]=etat;
	nativ_setEtatModule(objPeer,indexPiece,indexMod,etat);

    }
    public void setDimMod(int indexPiece,int indexMod,int dim){
	    nativ_setDimLumiere(objPeer,indexPiece,indexMod,dim);
    }
    public void setBrightMod(int indexPiece,int indexMod,int bright){
	    nativ_setBrightLumiere(objPeer,indexPiece,indexMod,bright);
    }
    public void setProgressMod(int indexPiece,int indexMod,int bright){
	    nativ_setProgressionVolet(objPeer,indexPiece,indexMod,bright);
    }
}
