package homein.client.android;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;

public class DataBase extends SQLiteOpenHelper {

    // Version de la base de données
    private static final int DATABASE_VERSION = 1;

    // Nom de la base
    private static final String COURSE_BASE_NAME = "Network.db";

    // Nom de la table
    public static final String COURSE_TABLE_NAME = "network";

    // Description des colonnes
    public static final String COLUMN_IP = "Ip";
    public static final int NUM_COLUMN_IP = 0;
    public static final String COLUMN_PORT = "Port";
    public static final int NUM_COLUMN_PORT = 1;

    private String addr;
    private String port;

    // Requête SQL pour la création da la base
    private static final String REQUETE_CREATION_BDD = "(CREATE TABLE network ("
		       + "Ip TEXT,"
	+ "Port INTEGER);";

    /**
     * Constructeur
     * 
     * @param context
     * @param name
     * @param factory
     * @param version
     */
    public DataBase(Context context) {
	super(context, COURSE_BASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Création de la base
     */
    public void onCreate(SQLiteDatabase db) {	
	    db.execSQL("CREATE TABLE network(Ip varchar2(30),Port varchar2(30));");
	    db.execSQL("INSERT INTO network VALUES ('localhost',9000);");
    }

    /**
     * Mise à jour de la base
     */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	    db.execSQL("DROP TABLE " + COURSE_TABLE_NAME + ";");
	    onCreate(db);
    }
}
