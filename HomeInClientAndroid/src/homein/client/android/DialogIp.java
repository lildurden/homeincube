package homein.client.android;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
 
public class DialogIp extends Dialog  implements OnClickListener{
 
    private Button  okButton;
 
    private Button   cancelButton;

    private Context  ctx;
    private ReadyListener readyListener;
    
    private String Address="";
    private String Port; 
 
    public interface ReadyListener { 
	public void ready(String sAddr,String sPort); 
    } 
 
    public DialogIp(Context context, ReadyListener readyListener,
			      String Address,String Port) {
	super(context);
	ctx = context;
	this.readyListener = readyListener;
	this.Address=Address;
	this.Port=Port;
 
    }
 
    /**
     * @see android.app.Dialog#onCreate(android.os.Bundle)
     */
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	Log.d("TestApp", "Dialog created");
	setContentView(R.layout.dialogip);			
	okButton = (Button) findViewById(R.id.btvalide);
	cancelButton = (Button) findViewById(R.id.btannuler);			
	okButton.setOnClickListener(this);
	cancelButton.setOnClickListener(this);	
	TextView textAddress = (TextView) findViewById(R.id.valaddrip);
	TextView textPort = (TextView) findViewById(R.id.valnumport);
	textAddress.setText(Address);
	textPort.setText(Port);
    }
 
    public void onClick(View view) {
	switch (view.getId()) {
	case R.id.btannuler:
	    dismiss();
	    break;
	case R.id.btvalide:
	    try{			
		TextView textAddress = (TextView) findViewById(R.id.valaddrip);
		TextView textPort = (TextView) findViewById(R.id.valnumport);
 
 
 
		String sRetour =textAddress.getText()+","+textPort.getText();
 
		String sAddress = textAddress.getText()+"";
		String sPort = textPort.getText()+""; 
		//appelle de la methode readyListener.ready et on passe en valeur le formatage
		readyListener.ready(sAddress,sPort);
		dismiss();
	    }catch(Exception e){
		TextView textbastmp2 = (TextView) findViewById(R.id.addrip);
		textbastmp2.setText(e.toString());
	    }
	    break;
	}
    }
 
}
