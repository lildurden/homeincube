
package homein.client.android;
import java.io.Serializable;

import android.app.Activity;

import android.os.Bundle;

import android.util.Log;

import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import android.view.View.OnClickListener;
import android.view.View;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import android.content.Context;
import android.content.Intent;
import android.content.ContentValues;

import android.database.sqlite.SQLiteDatabase;
import android.database.Cursor;

public class HomeInClient extends Activity
{
    private static final String TAG = "TestInterface";

    private TextView Address;
    private Button SeConnecter;
    private Context ctx; 

    private DataBase db;
    private SQLiteDatabase bdd;

    private String addr;
    private String port;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
	
	ctx=this;
	Log.d(TAG, "onCreate()");

	Address= (TextView) findViewById(R.id.ip);
	SeConnecter = (Button) findViewById(R.id.connecter);
	SeConnecter.setOnClickListener(connectButton);
	Address.setText("");
    }
    
    OnClickListener connectButton = new OnClickListener(){
	    public void onClick(View V){
		try{
		    Toast.makeText(ctx,  "Ip: Port: ",Toast.LENGTH_SHORT).show();


		    Log.d(TAG, "onClick()");
		    db=new DataBase(ctx);
		    bdd=db.getWritableDatabase();
		    Cursor cur = bdd.query("network", 
					   null, null, null, null, null, null);
		    cur.moveToFirst();
		    while (cur.isAfterLast() == false) {

			addr=cur.getString(0);
			port=cur.getString(1);
			cur.moveToNext();
		    }
		    bdd.close();

		    Intent intent = new Intent ( ctx, RoomsView.class ) ;
		    intent.putExtra("Addr",addr);
		    intent.putExtra("Port",port);
		    ctx.startActivity (intent ) ;
		}
		catch(Exception e){
		}
	    }
	}; 

    public boolean onCreateOptionsMenu(Menu menu) {
 
        //Création d'un MenuInflater qui va permettre d'instancier un Menu XML en un objet Menu
        MenuInflater inflater = getMenuInflater();
        //Instanciation du menu XML spécifier en un objet Menu
        inflater.inflate(R.layout.menuprinc, menu);
 
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
	//On regarde quel item a été cliqué grâce à son id et on déclenche une action
	switch (item.getItemId()) {
	case R.id.option:
	    return true;
	case R.id.portnumber:
	    db=new DataBase(ctx);
	    bdd=db.getWritableDatabase();
	    Cursor cur = bdd.query("network", 
	    				  null, null, null, null, null, null);
	    cur.moveToFirst();
	    while (cur.isAfterLast() == false) {
		Toast.makeText(ctx, cur.getString(0), Toast.LENGTH_SHORT).show();
		Toast.makeText(ctx, cur.getString(1), Toast.LENGTH_SHORT).show();
		addr=cur.getString(0);
		port=cur.getString(1);
		cur.moveToNext();
	    }
	    DialogIp dialog = new DialogIp(ctx,new OnReadyListener(),
					   addr,port);
	    dialog.setTitle("Paramètres réseau");
	    dialog.show();
	    return true;
	case R.id.quitter:
	    //Pour fermer l'application il suffit de faire finish()
	    finish();
	    return true;
	}
	return false;
    }
    private class OnReadyListener implements DialogIp.ReadyListener
    { 
        public void ready(String sAddressIp,String sPortNumber) { 
	  
	    addr=sAddressIp;
	    port=sPortNumber;
	    bdd.execSQL("UPDATE network set Ip='"+addr+"';");
	    bdd.execSQL("UPDATE network set Port="+port+";");
	    bdd.close();
        }
     } 
    protected void onStart(){
	//Au démarrage de l'application

	super.onStart();
	//Toast.makeText(getBaseContext(), "onStart",Toast.LENGTH_SHORT).show();
	Log.d(TAG, "onStart()");
    }
    
    protected void onRestart(){
	super.onRestart();
	Log.d(TAG, "onRestart()");	
    }

    protected void onResume(){
	super.onResume();
	Log.d(TAG, "onResume()");
    }

    protected void onPause(){
	super.onPause();
	Log.d(TAG, "onStart()");
    }

    protected void onStop(){
	super.onStop();
	Log.d(TAG, "onStop()");
    }

    protected void onDestroy(){
	super.onDestroy();
	Log.d(TAG, "onDestroy()");
    }
    public void finish() {
        super.finish();
    }
}
