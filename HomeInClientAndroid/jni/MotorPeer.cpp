#include "MotorPeer.hpp"
#include "Moteur2Interface.hpp"
JNIEXPORT jlong JNICALL Java_homein_client_android_MotorPeer_nativ_1createPeer
(JNIEnv * env, jobject obj){
  Moteur2Interface * Motor=new Moteur2Interface();
  return reinterpret_cast<jlong>(Motor);
}

JNIEXPORT jboolean JNICALL Java_homein_client_android_MotorPeer_nativ_1Connect
(JNIEnv * env, jobject obj, jlong objMotor, jstring Addr, jint Port){
  Moteur2Interface * Motor=reinterpret_cast<Moteur2Interface*>(objMotor);
  return (jboolean)Motor->connect(env->GetStringUTFChars(Addr,0),(int)Port);
}

JNIEXPORT jboolean JNICALL Java_homein_client_android_MotorPeer_nativ_1ConnectUser
(JNIEnv * env, jobject obj, jlong objMotor, jstring name, jstring pswd){
  Moteur2Interface * Motor=reinterpret_cast<Moteur2Interface*>(objMotor);
  return (jboolean)Motor->connectUser(env->GetStringUTFChars(name,0),env->GetStringUTFChars(pswd,0));
}

JNIEXPORT jint JNICALL Java_homein_client_android_MotorPeer_nativ_1getNbRooms
(JNIEnv * env, jobject obj, jlong objMotor){
  Moteur2Interface * Motor=reinterpret_cast<Moteur2Interface*>(objMotor);
  return (jint)Motor->getNbPieces();
}

JNIEXPORT jstring JNICALL Java_homein_client_android_MotorPeer_nativ_1getNameRoom
(JNIEnv * env, jobject obj, jlong objMotor, jint indexPiece){
  Moteur2Interface * Motor=reinterpret_cast<Moteur2Interface*>(objMotor);
  return env->NewStringUTF(Motor->getNomPiece((int)indexPiece));
  //return env->NewStringUTF("Pti con");
}

JNIEXPORT jint JNICALL Java_homein_client_android_MotorPeer_nativ_1getIdRoom
(JNIEnv * env, jobject obj, jlong objMotor, jint indexPiece){
  Moteur2Interface * Motor=reinterpret_cast<Moteur2Interface*>(objMotor);
  return (jint)Motor->getIdPiece((int)indexPiece);
}

JNIEXPORT jint JNICALL Java_homein_client_android_MotorPeer_nativ_1getNbMods
(JNIEnv * env, jobject obj, jlong objMotor, jint indexPiece){
  Moteur2Interface * Motor=reinterpret_cast<Moteur2Interface*>(objMotor);
  return (jint)Motor->getNbModule((int)indexPiece);
}

JNIEXPORT jint JNICALL Java_homein_client_android_MotorPeer_nativ_1getIdMod
(JNIEnv * env, jobject obj, jlong objMotor, jint indexPiece, jint indexModule){
   Moteur2Interface * Motor=reinterpret_cast<Moteur2Interface*>(objMotor);
   return (jint)Motor->getIdModule((int)indexPiece,(int)indexModule);
}

JNIEXPORT jstring JNICALL Java_homein_client_android_MotorPeer_nativ_1getNomMod
(JNIEnv * env, jobject obj, jlong objMotor, jint indexPiece, jint indexModule){
   Moteur2Interface * Motor=reinterpret_cast<Moteur2Interface*>(objMotor);
   return env->NewStringUTF(Motor->getNomModule((int)indexPiece,(int)indexModule));
}

JNIEXPORT jint JNICALL Java_homein_client_android_MotorPeer_nativ_1getTypeMod
(JNIEnv * env, jobject obj, jlong objMotor, jint indexPiece, jint indexModule){
   Moteur2Interface * Motor=reinterpret_cast<Moteur2Interface*>(objMotor);
   return (jint)Motor->getTypeModule((int)indexPiece,(int)indexModule);
}

JNIEXPORT jboolean JNICALL Java_homein_client_android_MotorPeer_nativ_1getEtatMod
(JNIEnv * env, jobject obj, jlong objMotor, jint indexPiece, jint indexModule){
   Moteur2Interface * Motor=reinterpret_cast<Moteur2Interface*>(objMotor);
   return (jboolean)Motor->getEtatModule((int)indexPiece,(int)indexModule);
}

JNIEXPORT void JNICALL Java_homein_client_android_MotorPeer_nativ_1setEtatModule
(JNIEnv * env, jobject obj, jlong objMotor, jint indexPiece, jint indexModule, jboolean etat){
   Moteur2Interface * Motor=reinterpret_cast<Moteur2Interface*>(objMotor);
   Motor->setEtatModule((int)indexPiece,(int)indexModule,(bool)etat);
}

JNIEXPORT jint JNICALL Java_homein_client_android_MotorPeer_nativ_1getBrightLumiere
(JNIEnv * env, jobject obj, jlong objMotor, jint indexPiece, jint indexModule){
   Moteur2Interface * Motor=reinterpret_cast<Moteur2Interface*>(objMotor);
   return (jint)Motor->getBrightLumiere((int)indexPiece,(int)indexModule);
}

JNIEXPORT void JNICALL Java_homein_client_android_MotorPeer_nativ_1setBrightLumiere
(JNIEnv * env, jobject obj, jlong objMotor, jint indexPiece, jint indexModule, jint bright){
   Moteur2Interface * Motor=reinterpret_cast<Moteur2Interface*>(objMotor);
   Motor->setBrightLumiere((int)indexPiece,(int)indexModule,(int)bright);
}

JNIEXPORT jint JNICALL Java_homein_client_android_MotorPeer_nativ_1getDimLumiere
(JNIEnv * env, jobject obj, jlong objMotor, jint indexPiece, jint indexModule){
   Moteur2Interface * Motor=reinterpret_cast<Moteur2Interface*>(objMotor);
   return (jint)Motor->getDimLumiere((int)indexPiece,(int)indexModule);
}

JNIEXPORT void JNICALL Java_homein_client_android_MotorPeer_nativ_1setDimLumiere
(JNIEnv * env, jobject obj, jlong objMotor, jint indexPiece, jint indexModule, jint dim){
   Moteur2Interface * Motor=reinterpret_cast<Moteur2Interface*>(objMotor);
   Motor->setDimLumiere((int)indexPiece,(int)indexModule,(int)dim);
}

JNIEXPORT jint JNICALL Java_homein_client_android_MotorPeer_nativ_1getProgressionVolet
(JNIEnv * env, jobject obj, jlong objMotor, jint indexPiece, jint indexModule){
   Moteur2Interface * Motor=reinterpret_cast<Moteur2Interface*>(objMotor);
   return (jint)Motor->getProgressionVolet((int)indexPiece,(int)indexModule);
}

JNIEXPORT void JNICALL Java_homein_client_android_MotorPeer_nativ_1setProgressionVolet
(JNIEnv * env, jobject obj, jlong objMotor, jint indexPiece, jint indexModule, jint progr){
   Moteur2Interface * Motor=reinterpret_cast<Moteur2Interface*>(objMotor);
   Motor->setProgressionVolet((int)indexPiece,(int)indexModule,(int)progr);
}
