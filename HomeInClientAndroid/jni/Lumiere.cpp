#include "Lumiere.hpp"
#include "Parser.hpp"

using namespace std;

Lumiere::Lumiere (int idC, string nom, int idPieceC, bool etatC, int dimC, int brightC):Module(idC,nom,idPieceC,etatC){
	dim = dimC;
	bright =brightC;

}

int Lumiere::getType(){
	return TYPE_LUMIERE;
}

int Lumiere::getDim(){
	return dim;
}
int Lumiere::getBright(){
	return bright;
}
void Lumiere::setDim(int D){
	dim=D;
	Parser* machin = Parser::get();

	machin->setModule(this);
}
void Lumiere::setBright(int B){
	bright=B;
	Parser::get()->setModule(this);
}
Lumiere & Lumiere::operator=(const Lumiere & source){
	dim=source.dim;
	bright=source.bright;
	return * this;
}

string Lumiere::toString() {
	ostringstream s;
	s << "Lumiere " << getId() << " : "
			<< getNom() << " est dans la piece "
			<< getIdPiece() << " et sont état est "
			<< getEtat()
			<< " dim  : " << dim
			<< " bright : " << bright << endl;
	return s.str();
}

Lumiere::~Lumiere() {

}
