// Protection contre la redefinition de la classe
#ifndef _PIECE_HPP
#define _PIECE_HPP

// On inclue les librairies et les headers necessaires
#include <string>
#include <vector>
#include "Module.hpp"

/**
 * Classe qui représente une piece.
 */
class Piece{

private:

	/**
	 * Identifiant de la piece
	 */
	int idPiece;

	/**
	 * Nom de la piece
	 */
	std::string nom;

	/**
	 * Nombre de module dans la piece
	 */
	int nbModule;

	/**
	 * Nombe d'angle dans la piece
	 */
	int nbAngle;

	/**
	 * Liste des modules
	 */
	std::vector<Module*> ListMod;

	/**
	 * Vrai si modules chargés
	 */
	bool loadedModules;

public:

	/**
	 * retourne un représentation de la piece en chaine de caractere
	 */
	std::string toString();

	/**
	 * constructeur
	 */
	Piece(int,std::string,int,int);

	/**
	 * Destructeur
	 */
	~Piece();

	/**
	 * Retourne un module Si les modules ne sont pas chargé charge automatiquement les modules
	 */
	Module *getModuleByIndex(int idMod);

	/**
	 * Accesseur qui retourne l'id de la piece
	 */
	int getIdPiece();

	/**
	 * Retourne le nom de la piece
	 */
	const char * getNom();

	/**
	 * Accesseur qui retourne le nombre de module de la piece
	 */
	int getNbModule();

	/**
	 * Accesseur qui retourne le nombre d'angle de la piece de la piece
	 */
	int getNbAngle();

	/**
	 * Surcharge de l'opérateur d'egalité
	 */
	Piece operator = (const Piece &);

	/**
	 * Actualise la liste des modules et le nombre de module
	 */
	void actualise();

};

#endif
