#include "TCP.hpp"
#include <iostream>
bool TCP::instancied = false;
TCP* TCP::instance;

TCP * TCP::get() {
  if(!instancied) {
    instance = new TCP();
    instancied = true;
  }
  return instance;
}

TCP::TCP() {

  typeTrame = -1;

}
bool TCP::connexion(const char* add, int port) {
  Port=port;
  adresse=new char[strlen(add)+1];
  strcpy(adresse,add);
  fd=socket(AF_INET,SOCK_STREAM,0);
  char * truc;
  if (fd<0){
    return  false;
  }
  //Recuperation adresse Serveur
  serveur=gethostbyname(adresse);
  if (serveur==NULL){
    return false;
  }
  printf("Socket OK et serveur OK\n");
  //Positionnement des infos serveur
  AdresseServeur.sin_family=AF_INET;
  AdresseServeur.sin_port=htons(Port);

  //Si utlisation de "gethostbyname"
  AdresseServeur.sin_addr=*(struct in_addr*)serveur->h_addr;
  Res=connect(fd,(struct sockaddr*)&AdresseServeur,sizeof(AdresseServeur));
  if(Res<0){
    cout<<"t'es nul"<<endl;
    return false;
  }
  else {
    truc = new char[strlen("OK")+1];
    return true;
  }
}
void TCP::sended(const char* ctrame) {
  string trame = ctrame;
  if(trame.substr(1,14) == "userConnection") {
    typeTrame = 3;
  } else if(trame.substr(1,9) == "getPieces") {
    typeTrame = 1;
  } else if(trame.substr(1,10) == "getModules") {
    typeTrame = 2;
  } else if(trame.substr(1,3) == "set") {
    typeTrame = 0;
  } else {
    exit(EXIT_FAILURE);
    typeTrame = -1;
  }
  SizeBufferTX=strlen(ctrame);
  send(fd,trame.c_str(),SizeBufferTX,0);
}
const char* TCP::recieve() {
  clearBufferRX();
  Res=recv(fd,BufferRX,255,0);
  ToSend=new char[strlen(BufferRX)+1];
  strcpy(ToSend,BufferRX);
  return ToSend;
}

TCP::~TCP(){
  if(adresse)
    delete adresse;
}

