#include "Volet.hpp"
#include "Parser.hpp"

using namespace std;


Volet::Volet (int idC, string nom, int idPieceC, bool etatC,int progressionC): Module(idC, nom, idPieceC, etatC){
	progression = progressionC;
}

int Volet::getProgression(){
	return progression;
}


void Volet::setProgression( int value){
	progression = value;
	Parser::get()->setModule(this);
}

int Volet::getType() {
	return TYPE_VOLET;
}

string Volet::toString() {
	ostringstream s;
	s << "Volet " << getId() << " : "
			<< getNom() << " est dans la piece "
			<< getIdPiece() << " et sont état est "
			<< getEtat()
			<< " progression  : " << progression
			<< " bright : " << endl;
	return s.str();
}

Volet::~Volet() {

}
