#include <iostream>
#include <string>
#include "Moteur2Interface.hpp"
#include "ListPieces.hpp"

using namespace std;


int main(int argc, char** argv) {

	string s;

	// On crée le gestionnaire du moteur
	Moteur2Interface* m2i = new Moteur2Interface();

	// On se connecte
	cout << "****** Connection au serveur ******" << endl;
	if(!m2i->connect("192.168.0.13", 5000)){
	  cout<<"Pas de connexion"<<endl;
	  return 0;
	}
	cout << endl;

	// On connecte l'utilisateur
	//cout << "****** Connection au serveur User ******" << endl;
	//m2i->connectUser("moi", "AZERTY");
	//cout << endl;

	// On charge la liste des pieces
	cout << "****** chargement liste des pieces ******" << endl;
	m2i->getNbPieces();
	s = ListPieces::get()->toString();
	cout << "ListPieces::toString() :" << endl
			<< s << endl;
	//m2i->connect("127.168.1.1", 80);
	cout << endl;

	// On charge la liste des module de la piece 1
	cout << "****** chargement liste des module piece 1 ******" << endl;
	m2i->getIdModule(1, 0);
	s = ListPieces::get()->toString();
	cout << "ListPieces::toString() :" << endl
			<< s << endl;

	// On change l'etat d'un module
	cout << "****** Modification scie ******" << endl;
	m2i->setEtatModule(1,0,true);
	s = ListPieces::get()->toString();
	cout << "ListPieces::toString() :" << endl
			<< s << endl;

	// On change l'etat d'un module
	cout << "****** Modification lumiere ******" << endl;
	m2i->setBrightLumiere(1,1,255);
	s = ListPieces::get()->toString();
	cout << "ListPieces::toString() :" << endl
			<< s << endl;

}
