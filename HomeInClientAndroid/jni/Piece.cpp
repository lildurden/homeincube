// On inclue le header de la classe
#include "Piece.hpp"
#include <sstream>
#include "Parser.hpp"

using namespace std;

string Piece::toString() {
	unsigned int i;
	string str;
	ostringstream s;
	s << "piece " << idPiece << " nom : " << nom
			<< " : nbModule = " << nbModule
			<< " : nbAngle = " << nbAngle << endl;
	str = s.str();
	for (i=0; i < ListMod.size(); i++) {

		str += ListMod[i]->toString();

	}
	return str;

}

Piece::Piece(int id, string nom, int nbMod,int nbAng){
	idPiece=id;
	this->nom = nom;
	nbModule=nbMod;
	nbAngle=nbAng;
	loadedModules = false;
}


Piece::~Piece(){
	ListMod.~vector();
}

const char * Piece::getNom(){return nom.c_str();}

int Piece::getIdPiece(){return idPiece;}

int Piece::getNbModule(){return nbModule;}

int Piece::getNbAngle(){return nbAngle;}

Piece Piece::operator = (const Piece & piece){
	idPiece=piece.idPiece;
	nbModule=piece.nbModule;
	nbAngle=piece.nbAngle;
	return *this;
}

Module * Piece::getModuleByIndex(int idMod){

	if(!loadedModules) {

		ListMod = Parser::get()->getModules(idPiece);
		nbModule = ListMod.size();
		loadedModules = false;

	}

	switch(ListMod[idMod]->getType()){
	case TYPE_PRISE:
		return (Prise*)ListMod[idMod];
		break;
	case TYPE_LUMIERE:
		return (Lumiere*)ListMod[idMod];
		break;
	case TYPE_VOLET:
		return (Volet*)ListMod[idMod];
		break;
	default:
		return NULL;
		break;
	}
	return NULL;
}

void Piece::actualise() {
	ListMod = Parser::get()->getModules(idPiece);
	nbModule = ListMod.size();
	loadedModules = true;
}

