/*
 * User.hpp
 *
 *  Created on: 5 janv. 2012
 *      Author: foxnounours
 */

#ifndef _USER_HPP
#define _USER_HPP

#include <string>

class User {

private:

	std::string nom;
	std::string mdp;

public:

	User(std::string, std::string);
    std::string getNom();
    std::string getMdp();
    bool connect();

};

#endif /* USER_HPP_ */
