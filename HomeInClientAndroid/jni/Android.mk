LOCAL_PATH:= $(call my-dir)

# first lib, which will be built statically
#
include $(CLEAR_VARS)

LOCAL_MODULE    := HomeInMotor
LOCAL_SRC_FILES := TCP.cpp ListPieces.cpp Parser.cpp Lumiere.cpp Volet.cpp Piece.cpp Module.cpp Prise.cpp connection.cpp User.cpp Moteur2Interface.cpp MotorPeer.cpp
LOCAL_LDLIBS    := -llog
include $(BUILD_SHARED_LIBRARY)
