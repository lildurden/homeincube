/*
 * User.cpp
 *
 *  Created on: 5 janv. 2012
 *      Author: foxnounours
 */

#include "User.hpp"
#include "Parser.hpp"

using namespace std;

User::User(std::string nom, std::string mdp) {

	this->nom = nom;
	this->mdp = mdp;

}

string User::getNom()
{
    return nom;
}

string User::getMdp()
{
    return mdp;
}

bool User::connect() {

	return Parser::get()->connect(this);

}

