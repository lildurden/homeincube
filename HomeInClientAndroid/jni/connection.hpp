/*
 * connection.hpp
 *
 *  Created on: 5 janv. 2012
 *      Author: foxnounours
 */

#ifndef _CONNECTION_HPP
#define _CONNECTION_HPP

#include <string>

class Connection {

private:

	std::string adresse;
	int port;

public:

	Connection(std::string, int);
	bool connect();

};

#endif /* _CONNECTION_HPP */
