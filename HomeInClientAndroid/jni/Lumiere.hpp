#ifndef _LUMIERE_HPP
#define _LUMIERE_HPP
#define TYPE_LUMIERE 2

#include <string>
#include "Module.hpp"

class Lumiere : public Module {
private:
	int dim;
	int bright;

public :
	Lumiere (int, std::string, int, bool, int,int);
	int getDim();
	int getBright();
	void setDim(int);
	void setBright(int);
	Lumiere & operator=(const Lumiere & source);
	virtual int getType();
	virtual std::string toString();
	virtual ~Lumiere();
};

#endif
