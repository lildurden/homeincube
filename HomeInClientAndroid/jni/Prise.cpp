#include "Prise.hpp"

using namespace std;

Prise::Prise(int idC , string nom, int idPieceC , bool etatC)
:Module(idC,nom,idPieceC,etatC){

}

int Prise::getType() {
	return TYPE_PRISE;
}

string Prise::toString() {
	ostringstream s;
	s << "Prise " << getId() << " : "
			<< getNom() <<  " est dans la piece "
			<< getIdPiece() << " et sont état est "
			<< getEtat() << endl;
	return s.str();
}

Prise::~Prise() {

}
