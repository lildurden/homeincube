#ifndef _PROTOCOLESQL_HPP
#define _PROTOCOLESQL_HPP
#include "SQLite.hpp"
#include <sstream>
class ProtocoleSQL
{
private:
  SQLite * IntSql;
public:
  ProtocoleSQL();
  ProtocoleSQL(const char * );
  ProtocoleSQL(SQLite * );
  
  vector<vector<string> >  Requete(string);
  vector<vector<string> > ListePieces();
  vector<vector<string> > ListeAngles(int IdPiece);
  vector<vector<string> > ListeModules(int IdPiece);
  vector<vector<string> > SetEtatModule(int IdModule,int etat);
  vector<vector<string> > SetProgressModule(int IdModule,int Progress);
  vector<vector<string> > SetDimModule(int IdModule,int Dim);
  vector<vector<string> > SetBrightModule(int IdModule,int Bright);

  vector<vector<string> > SetEtat(bool etat);
  vector<vector<string> > GetModule(int IdModule);
  vector<vector<string> > GetDatas(int IdModule);
  vector<vector<string> > GetDim(int IdModule);
  vector<vector<string> > GetBright(int IdModule);
  vector<vector<string> > GetProgress(int IdModule);


  string itos(int i);
  string btos(bool i);
};

#endif // _PROTOCOLESQL_HPP
