#ifndef _SQLITE_HPP
#define _SQLITE_HPP
#include <iostream>
#include <cstdio>
#include <sqlite3.h>
#include <cstring>
#include <cstdlib>
#include <vector>

using namespace std;

class SQLite{
private:
  sqlite3 * dbQuiContientTout;

public:
  SQLite(const char *);
  ~SQLite();
  int Open(const char * nomdb);
  vector<vector<string> > query(const char* query);

  int Close();
};

#endif // _SQLITE_HPP
