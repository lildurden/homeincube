#ifndef _HOMEINTCP_SERVER_HPP
#define _HOMEINTCP_SERVER_HPP

#include "HomeInTcp.hpp"

class HomeInTcp_Server{
private:
  HomeInTCP * HomeInConnexion;
  int HomeIn_Socket;
  int HomeIn_FdClient;

public:
  HomeInTcp_Server();
  HomeInTcp_Server(int);
  int HITCP_Server_init(int);
  int HITCP_Server_init(int,int);
  int HITCP_Server_letsGo();
  void HITCP_Server_send(const char*,int);
  const char * HITCP_Server_recv(int);
};


#endif
