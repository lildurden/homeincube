#ifndef _HOMEINTCP_HPP
#define _HOMEINTCP_HPP

#include <cstdio>
#include <cstdlib>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <ctype.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cstring>
#include <fcntl.h>
#include <pthread.h>

#include <iostream>
using namespace std;

class HomeInTCP{
private:
  int HomeIn_simultaneousClients;
  int HomeIn_socket;
  int HomeIn_portNumber;

  struct sockaddr_in HomeIn_serverAddress;
  int HomeIn_sizeServerAddress;

  int fdClient;
  //int Res;
  pthread_t thread;
  pthread_mutex_t mutex;
	
  struct sockaddr AdresseClient;
  char BufferRx[255];
  char BufferTx[255];
	
  socklen_t TailleAdresseClient;
  int NbTramesRx;
	
public:
  /*************************************************************
   *                                                           *
   *   Constructeur de la classe TCP                           *
   *    Initialise NbTrameRX, TailleAdreeseClient, NumPort et  *
   *    MaSocket                                               *
   *                                                           * 
   *************************************************************/
  HomeInTCP();
  HomeInTCP(int);

  /*************************************************************
   *                                                           *
   *  Permet de définir un numéro de port à écouter            *
   *                                                           *
   *************************************************************/
  void HITCP_Set_port(int);
	
  /*************************************************************
   *                                                           *
   *  Création de la Socket                                    *
   *       Retourne -1 en cas d'échec, 0 en cas de réussite    *
   *                                                           *
   *************************************************************/
  int HITCP_Create_socket();

  /*************************************************************
   *                                                           *
   *  Retourne le descripteur de la socket                     *
   *                                                           *
   *************************************************************/
  int HITCP_Get_Descripteur_socket();
	
  /*************************************************************
   *                                                           *
   *  Nommage de la socket                                     *
   *          Retourne -5 si le numéro de port est 0,          *
   *                   -1 en cas d'erreur de nommage,          *
   *                    0 si tout va bien                      *
   *                                                           *
   *************************************************************/
  int HITCP_Named_socket();

  /*************************************************************
   *                                                           *
   *  Mise en écoute de la socket                              *
   *          Retourne -5 si le numéro de port est 0,          *
   *                   -1 en cas d'erreur de nommage,          *
   *                    0 si tout va bien                      *
   *                                                           *
   *************************************************************/
  int HITCP_Listened_socket();

  void HITCP_Set_simultaneousClients(int);

  int HITCP_Have_Client();
 
  void HITCP_Send(const char *,int);
  
  const char * HITCP_Recv(int);

  int Client();
	
};


#endif


