#ifndef _HOMEINTRAITEMENT_HPP
#define _HOMEINTRAITEMENT_HPP

#include "HomeInTcp_Server.hpp"
#include "ProtocoleSQL.hpp"
#include "ProtocolePLC.hpp"

#include <cstring>
#include <cstdlib>


class HomeInTraitement{
private:
  ProtocolePLC * PLC;
  ProtocoleSQL * SQL;
  HomeInTcp_Server * Server;
  string * splitRes;
  int splitSize;
  int HomeIn_FdClient;
  string getServer;
  string toClient;
  vector<vector<string> > res;
  pthread_mutex_t mutex;
  pthread_t thread;
public:
  HomeInTraitement();
  
  bool setServer(int,int);
  string getRecv(int);

  bool recvServer(int );
  
  bool sendServer(const char *,int);
  
  string * split(string str2split, int* tSize);

  bool traitement(int);
  
  int parseInt(string s);
  pthread_t get_thread(){return thread;}
  pthread_mutex_t * get_mutex(){return &mutex;}

};

#endif
