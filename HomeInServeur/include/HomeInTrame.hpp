#ifndef _HOMEINTRAME_HPP
#define _HOMEINTRAME_HPP

#include "HomeInTcp_Server.hpp"
#include "ProtocoleSQL.hpp"
#include <cstring>
#include <cstdlib>


class HomeInTrame{
private:
 
  int fd;
  pthread_mutex_t mutex;
  pthread_t thread;
public:
  HomeInTrame(){}
  void setMutex(pthread_mutex_t mut){mutex=mut;}
  void setFd(int fdc){fd=fdc;}
  int getFd(){return fd;}
  pthread_mutex_t get_mutex(){return mutex;}

};

#endif
