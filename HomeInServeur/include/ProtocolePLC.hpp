#ifndef PROTOCOLEPLC_HPP
#define PROTOCOLEPLC_HPP

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "RS232.hpp"

using namespace std;

class ProtocolePLC{
private:
  //Trame finales TX/RX
  unsigned char TrameTX[8];
  unsigned char TrameRX[9];
  //Découpage trame TX
  unsigned char STX;
  unsigned char Length;
  unsigned char ETX;
  //partie data
  unsigned char UsrCode;
  unsigned char Home_Unit;
  unsigned char Cmd;
  unsigned char Data1;
  unsigned char Data2;
	
  //Commandes
  unsigned char AllUnitsOFF;
  unsigned char AllLtsOn;
  unsigned char On;
  unsigned char Off;
  unsigned char Dim;
  unsigned char Bright;
  unsigned char AllLightsOff;
  unsigned char AllUserltsOn;
  unsigned char AllUserUnitOff;
  unsigned char AllUserLightOff;
  unsigned char Blink;
  unsigned char FadeStop;
  unsigned char PresetDin;
  unsigned char StatusOn;
  unsigned char StatusOff;
  unsigned char StatusReq;

  RS232 * SerialPLC;
  void initCommande();
	
public:
  ProtocolePLC(RS232 * );
  //definition des partie de la trame TX
  void InsertUserCode(unsigned char usrcd);
  void InsertHomeUnit(unsigned char HomeUnit);
  void InsertCommand(unsigned char Command);
  void InsertData1(unsigned char Dat1);
  void InsertData2(unsigned char Dat2);
  //definition de la trame TX
  void SetTrameTx();
  bool DefHomeUnit(unsigned char ,int);
  bool ChkHome(unsigned char);
  bool ChkUnit(int );
  unsigned char* GetTrameTX();
  void SetCommande(int);
  bool defAll(unsigned char,unsigned char,int,int,unsigned char,unsigned char);
  bool TxRS232();
  bool RxRS232();
  unsigned char * GetTrameRX();
  unsigned char fBackUsrCode(){return TrameRX[2];}
  unsigned char fBackHomeUnit(){return TrameRX[3];}
  unsigned char fBackCommand(){return TrameRX[4];}
  unsigned char fBackData1(){return TrameRX[5];}
  unsigned char fBackData2(){return TrameRX[6];}
  bool checkFeedBack();

};

#endif
