#ifndef RS232_HPP
#define RS232_HPP

#include <iostream>
#include <stdio.h>
#include <fcntl.h>
#include <termios.h>
#include <stdlib.h>
#include <string.h>
using namespace std;
#include <errno.h>

class RS232{
private:
  int fd;
  char * SerialP;
  struct termios port_settings;
  bool ouverture;
public:
  RS232();
  RS232(char * );
  ~RS232(){
    if(SerialP)
      delete SerialP;
  }
  bool DefParamPort();
  bool RxTrame(unsigned char [9]);
  bool TxTrame(unsigned char * );
  bool TxXbee(unsigned char * Trame);
  bool getOuverture(){ return ouverture;}
};

#endif
