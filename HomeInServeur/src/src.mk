SRC=$(wildcard $(ROOT)/$(DIR_SRC)/*.cpp)
OBJ=$(SRC:$(ROOT)/$(DIR_SRC)/%.c=$(ROOT)/$(DIR_OBJ)/%.o)

# Compilation of the program
$(EXEC): $(OBJ) #$(DYNAMIC_LIBS:%=$(ROOT)/$(DIR_LIB)/%.so)
	$(GXX) -o $@ $^ $(CFLAGS) -lpthread -lsqlite3

# Compilation from sources (.cpp) to objects (.o)
$(ROOT)/$(DIR_OBJ)/%.o:: $(ROOT)/$(DIR_SRC)/%.cpp
	$(GXX) -c -o $@ $< $(CFLAGS_OBJ) 
