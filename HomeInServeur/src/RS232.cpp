#include "RS232.hpp"
RS232::RS232(){
  ouverture=true;
  fd=open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY);
  if(fd<0){
    cerr<<"Erreur ouverture du port serie: /dev/ttyUSB0"<<endl;
    ouverture=false;
  }
  if(!DefParamPort()){
    cerr<<"Erreur definition des parametres série"<<endl;
    ouverture=false;
  }
  else ouverture=true;
}
RS232::RS232(char * Serial){
  ouverture=true;
  cout<<Serial<<endl;
  fd=open(Serial,  O_RDWR | O_NOCTTY | O_NDELAY);
  if(fd<0){
    cerr<<"Erreur ouverture du port serie: "<<Serial<<endl;
    ouverture=false;
  }
  if(!DefParamPort()){
    cerr<<"Erreur definition des parametres série"<<endl;
    ouverture=false;
  }
  else ouverture=true;
}
bool RS232::DefParamPort(){
  if (  (tcgetattr(fd, &port_settings)) < 0 )
    return false;	
  bzero(&port_settings, sizeof(port_settings));
	
  port_settings.c_cflag = (B9600 | CS8 | CLOCAL | CREAD) ;
  port_settings.c_iflag = IGNPAR ;
  port_settings.c_oflag = 0;            // Pas de mode de sortie particulier (mode raw).
  port_settings.c_lflag = 0;            // non canonique
  port_settings.c_cc[VTIME] = 0;        /* inter-character timer unused */
  port_settings.c_cc[VMIN]  = 0;        /* blocking read until 1 character arrives */
	
  tcflush(fd, TCIFLUSH);
  if ( ( tcsetattr(fd,TCSANOW,&port_settings) < 0 ) )
    return false;
  return true;
	
}
bool RS232::TxTrame(unsigned char * Trame){
  int retval;
  //retval=write(fd,Trame,8);
  if(retval<0)
    return false;
  else return true;
}
bool RS232::TxXbee(unsigned char * Trame){
  int retval;
  //retval=write(fd,Trame,1);
  if(retval<0)
    return false;
  else return true;
}
bool RS232::RxTrame(unsigned char Rx[9]){
  //read(fd,Rx,9);
  return Rx;
}
