#include "HomeInTraitement.hpp"
#include "HomeInTrame.hpp"
#include <vector>
#include <pthread.h>
void * ProtoThread(void * data);

HomeInTraitement::HomeInTraitement(){
  Server=new HomeInTcp_Server();
  splitSize=0;
  
  SQL=new ProtocoleSQL();
  //PLC=new ProtocolePLC(new RS232("/dev/ttyUSB0"));
  /* pthread_mutex_init(&mutex,NULL);*/
}
  
bool HomeInTraitement::setServer(int port,int nbClient){

  Server=new HomeInTcp_Server();
  splitSize=0;
  SQL=new ProtocoleSQL();
  pthread_mutex_init(&mutex,NULL);
  
  HomeInTrame * HiT=new HomeInTrame();
  Server->HITCP_Server_init(nbClient,port);
  while(1){
    HomeIn_FdClient=Server->HITCP_Server_letsGo();
    if(HomeIn_FdClient<0){
      return -1;
    }
    //HomeInTrame * HiT=new HomeInTrame();
    HiT->setMutex(mutex);
    HiT->setFd(HomeIn_FdClient);
    cout<<"avant thread"<<endl;
    pthread_create(&thread,NULL,ProtoThread,(void*)HiT);
      
  }
  pthread_join(thread,NULL);
  pthread_exit(NULL);
}

bool HomeInTraitement::recvServer(int fd){
  getServer=Server->HITCP_Server_recv(fd);
  cout<<"recv "<<getServer<<endl;
  if(getServer=="-1"){
    return false;
  }
  else{
    return true;
  }
  //cout<<"recv "<<getServer<<endl;
}
  
bool HomeInTraitement::sendServer(const char* toSend,int fd){
  Server->HITCP_Server_send(toSend,fd);
  cout<<toSend<<endl;
}
 
bool HomeInTraitement::traitement(int fdClient){
  
  if(!recvServer(fdClient)){
    cout<<"return traitemeny"<<endl;
    return false;
  }
  splitRes=split(getServer,&splitSize);

  if(strcmp(splitRes[0].c_str(),"getPieces")==0){
    res=SQL->ListePieces();
    
    toClient="<getPieces>";
    toClient+="<";
    toClient+=SQL->itos(res.size());
    toClient+="><";
    //nombre <id nom nbmod nbangle>
    for(vector<vector<string> >::iterator it = res.begin();
	it < res.end(); ++it){
      vector<string> row = *it;
      toClient+="<<";
      toClient+=row.at(0);
      toClient+="><";
      toClient+=row.at(1);
      toClient+="><";
      toClient+=row.at(2);
      toClient+="><";
      toClient+=row.at(3);
      toClient+=">>";      
    }
    toClient+=">";
    sendServer(toClient.c_str(),fdClient);
  }
  if(strcmp(splitRes[0].c_str(),"getModules")==0){
    //cout<<"split Mods  "<<splitRes[1]<<endl;
    res=SQL->ListeModules(parseInt(splitRes[1]));
    
    toClient="<getModules>";
    toClient+="<";
    toClient+=splitRes[1];
    toClient+="><";
    toClient+=SQL->itos(res.size());
    toClient+="><";
    //nombre <id nom nbmod nbangle>
    for(vector<vector<string> >::iterator it = res.begin();
	it < res.end(); ++it){
      vector<string> row = *it;
      toClient+="<<";
      toClient+=row.at(0);
      toClient+="><";
      toClient+=row.at(1);
      toClient+="><";
      toClient+=row.at(2);
      toClient+="><";
      toClient+=row.at(3);
      //toClient+=">";
      vector<vector<string> > resTmp;
      vector<vector<string> >::iterator itTmp; 
	vector<string> rowTmp;
      switch(parseInt(row.at(2))){
      case 0:
	break;
      case 1:
	break;
      case 2:
	resTmp=SQL->GetDim(parseInt(row.at(0)));
	itTmp= resTmp.begin(); 
	rowTmp = *itTmp;

	toClient+="><";
	toClient+=rowTmp.at(0);
	resTmp=SQL->GetBright(parseInt(row.at(0)));
	itTmp= resTmp.begin(); 
	rowTmp = *itTmp;
	toClient+="><";
	toClient+=rowTmp.at(0);
	
	break;
      case 3:
  	resTmp=SQL->GetProgress(parseInt(row.at(0)));

	itTmp= resTmp.begin();
	rowTmp = *itTmp;
	toClient+="><";
	toClient+=rowTmp.at(0);
	
	break;
      }
      /*
      if(parseInt(row.at(2))>1){
	vector<vector<string> > resTmp=SQL->GetDatas(parseInt(row.at(0)));
	for(vector<vector<string> >::iterator itTmp= resTmp.begin(); itTmp > resTmp.end(); ++itTmp){
	  vector<string> rowTmp = *itTmp;
	  toClient+="><";
	  toClient+=rowTmp.at(2);
	}				     
	}*/
      toClient+=">>";      
    }
    toClient+=">";
    //cout<<toClient<<endl;
    sendServer(toClient.c_str(),fdClient);
    cout<<"Sended"<<endl;
    //sendServer("<getModules><1><1><<<2><scie sauteuse><1><0>><<3><lumiere bain d'acide><2><1><100><50>>>");
  }
  if(strcmp(splitRes[0].c_str(),"setModule")==0){
    

    res=SQL->GetModule(parseInt(splitRes[2]));
    int cmd;
    cout<<parseInt(splitRes[3])<<endl;
    if(parseInt(splitRes[3])){
      cmd=3;
    }
    else
      cmd=4;
    for(vector<vector<string> >::iterator it = res.begin();
	it < res.end(); ++it){
      vector<string> row = *it;
      char tmp[3];
      int IdModule=atoi(row.at(0).c_str());//IdModule
      int CodeUnit=atoi(row.at(4).c_str());
      //int etat=atoi(row.at(4).c_str());
      //int dim=atoi(row.at(5).c_str());
      //bright=atoi(row.at(6).c_str());
      strcpy(tmp,row.at(1).c_str());
      //cout<< row.at(7)<<endl;
      unsigned char Home=atoi(tmp);
      strcpy(tmp,row.at(2).c_str());
      unsigned char User=atoi(tmp);
      //strcpy(tmp,row.at(1).c_str());
      //Proto=atoi(tmp);

      Home='A';
      cout<<row.at(0)<<endl<<row.at(1)<<endl<<row.at(2)<<endl<<row.at(3)<<endl<<row.at(4)<<endl<<row.at(5)<<endl<<row.at(6)<<endl;
      //PLC->defAll(User,Home,CodeUnit,cmd,255,100);

    }
    SQL->SetEtatModule(parseInt(splitRes[2]),parseInt(splitRes[3]));
  }
  if(strcmp(splitRes[0].c_str(),"setPrise")==0){
    
    res=SQL->GetModule(parseInt(splitRes[2]));
    int cmd;
    if(parseInt(splitRes[3])){
      cmd=3;
    }
    else
      cmd=4;
    for(vector<vector<string> >::iterator it = res.begin();
	it < res.end(); ++it){
      vector<string> row = *it;
      char tmp[3];
      int IdModule=atoi(row.at(0).c_str());//IdModule
      int CodeUnit=atoi(row.at(4).c_str());
      //int etat=atoi(row.at(4).c_str());
      //int dim=atoi(row.at(5).c_str());
      //bright=atoi(row.at(6).c_str());
      strcpy(tmp,row.at(1).c_str());
      //cout<< row.at(7)<<endl;
      unsigned char Home=atoi(tmp);
      strcpy(tmp,row.at(2).c_str());
      unsigned char User=atoi(tmp);
      //strcpy(tmp,row.at(1).c_str());
      //Proto=atoi(tmp);

      Home='A';
      cout<<row.at(0)<<endl<<row.at(1)<<endl<<row.at(2)<<endl<<row.at(3)<<endl<<row.at(4)<<endl<<row.at(5)<<endl<<row.at(6)<<endl;
      //PLC->defAll(User,Home,CodeUnit,cmd,255,100);

    }
    SQL->SetEtatModule(parseInt(splitRes[2]),parseInt(splitRes[3]));
  }
  if(strcmp(splitRes[0].c_str(),"setLumiere")==0){
    
    vector<vector<string> > resTmp; 
    vector<vector<string> >::iterator itTmp; 
    vector<string> rowTmp;

    vector<vector<string> >::iterator it;
    vector<string> row;
    
    unsigned char order;
    unsigned char Data2,Data1;

    char tmp[3];


    res=SQL->GetModule(parseInt(splitRes[2]));
    it = res.begin();
    row = *it;
    
    resTmp=SQL->GetDim(parseInt(row.at(0)));
    itTmp= resTmp.begin(); 
    rowTmp = *itTmp;
    int Dim=atoi(rowTmp.at(0).c_str());

    resTmp=SQL->GetBright(parseInt(row.at(0)));
    itTmp = resTmp.begin();
    rowTmp = *itTmp;
    int Bright=atoi(rowTmp.at(0).c_str());


    int IdModule=atoi(row.at(0).c_str());//IdModule
    int CodeUnit=atoi(row.at(4).c_str());
    int dim=atoi(splitRes[4].c_str());
    int bright=atoi(splitRes[5].c_str());
    strcpy(tmp,row.at(1).c_str());
    unsigned char Home=atoi(tmp);
    strcpy(tmp,row.at(2).c_str());
    unsigned char User=atoi(tmp);

    if(Dim!=dim||Bright!=bright){
      order=13;
      Data1=dim;
      Data2=bright;
    }
    else{
      if(Dim!=dim&&Bright==bright){
	order=5;
	Data1=dim;
	Data2=0;
      }
      else{
	if(Dim==dim&&Bright!=bright){
	  order=6;
	  Data1=bright;
	  Data2=0;
	}
      }
    }
    

    Home='A';
    cout<<row.at(0)<<endl<<row.at(1)<<endl<<row.at(2)<<endl<<row.at(3)<<endl<<row.at(4)<<endl<<row.at(5)<<endl<<row.at(6)<<endl;
    //PLC->defAll(User,Home,CodeUnit,order,Data1,Data2);

  
    SQL->SetEtatModule(parseInt(splitRes[2]),parseInt(splitRes[3]));
    SQL->SetDimModule(parseInt(splitRes[2]),parseInt(splitRes[4]));
    SQL->SetBrightModule(parseInt(splitRes[2]),parseInt(splitRes[5]));
  }
  if(strcmp(splitRes[0].c_str(),"setVolet")==0){
    SQL->SetEtatModule(parseInt(splitRes[2]),parseInt(splitRes[3]));
    SQL->SetProgressModule(parseInt(splitRes[2]),parseInt(splitRes[4]));
  }
  return true;
}

string * HomeInTraitement::split(string str2split, int* tSize){

  // On déclare les variables locales
  unsigned int i, j = 0, taille = 0;
  int niveau = 0;
  string strAux;
  string *strTable;

  // On calcule la taille du tableau et on la recupere
  for (i = 0; i < str2split.size(); i++) {

    // Si on a un caractere ouvrant on augmente de niveau
    if ( str2split[i] == '<') {

      // Si le niveau est 0
      if(niveau == 0) {

	// On a un element de plus
	taille++;

      }

      niveau++;

    }
    // Si on a un caractere fermant on diminue de niveau
    else if ( str2split[i] == '>') {

      niveau--;

      // Si on arrive à un niveau negatif la trame est incorrecte
      if(niveau < 0) {

	cout << "Error in Parser::split(string, int*) : trame incorrecte" << endl;
	exit(EXIT_FAILURE);

      }

    }

  }

  // Si le niveau n'est pas égal a 0 à la fin on a une trame incorrecte
  if(niveau != 0) {

    cout << "Error in Parser::split(string, int*) : trame incorrecte" << endl;
    exit(EXIT_FAILURE);

  }

  // On enregistre la taille du tableau
  *tSize = taille;

  // On crée le tableau
  strTable = new string[taille];

  // On decoupe la trame
  for (i = 0; i < str2split.size(); i++) {

    // Si on a un caractere fermant on diminue de niveau
    if (str2split[i] == '>') {

      niveau--;

      // Si on est à la fin d'un element
      if(niveau == 0) {

	// On ajoute l'element et on reinitialise pour le suivant
	strTable[j] = strAux;
	j++;
	strAux = "";

      }

    }

    // Si c'est un autre caractere on l'ajoute a la chaine auxiliere
    if(niveau != 0){

      strAux += str2split[i];

    }

    // Si on a un caractere ouvrant on augmente de niveau
    if ( str2split[i] == '<') {

      niveau++;

    }

  }

  return strTable;

}

int HomeInTraitement::parseInt(string s) {

  istringstream ss(s);
  int i;
  if(!(ss >> i)) {

    cout << "Error in parseInt(string) : string incorrecte"
	 << endl;
    exit(EXIT_FAILURE);

  }

  return i;

}
void * ProtoThread(void * data){
  int Res;
  HomeInTrame * param=(HomeInTrame*)data;
  int fd= param->getFd();
  HomeInTraitement * tr =new HomeInTraitement();
  pthread_mutex_t  mutex=param->get_mutex();
  bool Traitres=true;
  while(Traitres){
    pthread_mutex_lock(&mutex);
    cout<<"klzeflkze"<<endl;
    Traitres=tr->traitement(fd);
    pthread_mutex_unlock(&mutex);
  }
}
