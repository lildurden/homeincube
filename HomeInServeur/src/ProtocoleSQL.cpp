#include "ProtocoleSQL.hpp"

ProtocoleSQL::ProtocoleSQL(){
  IntSql=new SQLite("HomeInDataBase.db");
}
ProtocoleSQL::ProtocoleSQL(const char * db){
  IntSql=new SQLite(db);
}
ProtocoleSQL::ProtocoleSQL(SQLite * db){
  IntSql=db;
}
vector<vector<string> > ProtocoleSQL::Requete(string requete){
  //cout<<"query debug 1 1 255 255 "<<requete<<endl;
  return IntSql->query (requete.c_str());
  //cout<<"fin query debug"<<endl;
}
vector<vector<string> > ProtocoleSQL::ListePieces(){
  string requete="SELECT IdPiece,NomPiece, NbModule, NbAngle FROM Piece;";
  return Requete(requete.c_str());
}
vector<vector<string> > ProtocoleSQL::ListeAngles(int IdPiece){
  string requete="SELECT Coordx, CoordY FROM AnglePiece WHERE IdPiece=";
  requete+=itos(IdPiece);
  requete+=";";
  return Requete(requete);
}
vector<vector<string> > ProtocoleSQL::ListeModules(int IdPiece){
  //cout<<"ProtocleSQL IdPiece  "<<IdPiece<<endl;
  string requete="SELECT IdModule,NomModule,TypeModule,Etat,IdData_Type, Home,CodeUnit FROM module WHERE IdPiece=";
  requete+=itos(IdPiece);
  requete+=";";
  return Requete(requete);
}
vector<vector<string> >  ProtocoleSQL::SetEtatModule(int IdModule,int etat){
  string requete="UPDATE Module set Etat=";
  requete+=itos(etat);
  requete+=" WHERE IdModule=";
  requete+=itos(IdModule);
  requete+=";";
  return Requete(requete);
}
vector<vector<string> >  ProtocoleSQL::SetProgressModule(int IdModule,int Progress){
  string requete="UPDATE Data set Value=";
  requete+=itos(Progress);
  requete+=" WHERE data.IdData='Progression' and data.IdModule=";
  requete+=itos(IdModule);
  requete+=";";
  cout<<requete<<endl;
  return Requete(requete);
}
vector<vector<string> >  ProtocoleSQL::SetDimModule(int IdModule,int Dim){
  string requete="UPDATE data set Value=";
  requete+=itos(Dim);
  requete+=" WHERE data.IdData='Dim' and data.IdModule=";
  requete+=itos(IdModule);
  requete+=";";
  cout<<requete<<endl;
  return Requete(requete);
}
vector<vector<string> >  ProtocoleSQL::SetBrightModule(int IdModule,int Bright){
  string requete="UPDATE data set Value=";
  requete+=itos(Bright);
  requete+=" WHERE data.IdData='Bright' and data.IdModule=";
  requete+=itos(IdModule);
  requete+=";";
  cout<<requete<<endl;

  return Requete(requete);
}
vector<vector<string> > ProtocoleSQL::SetEtat(bool etat){
  string requete="UPDATE module set Etat=";
  requete+=btos(etat);
  requete+=";";
  return Requete(requete);
}
vector<vector<string> > ProtocoleSQL::GetModule(int IdModule){
  string requete="SELECT IdModule,Home,User,IdPiece,CodeUnit,Etat,IdProtocole FROM Module, Home, User WHERE IdModule=";
  requete+=itos(IdModule);
  requete+=";";
  cout<<requete<<endl;
  return Requete(requete);
}
vector<vector<string> > ProtocoleSQL::GetDatas(int IdModule){
  string requete="select data.IdData, data_type.IdData_Type, value from data, data_type ,module where data.IdData=data_type.IdData and data.IdModule=module.IdModule and data_type.IdData_Type=module.IdData_Type and module.IdModule=";
  requete+=itos(IdModule);
  requete+=";";
  return Requete(requete);
}
vector<vector<string> > ProtocoleSQL::GetDim(int IdModule){
  string requete="select value from data where data.IdData='Dim' and data.IdModule=";
  requete+=itos(IdModule);
  requete+=";";
  cout<<requete<<endl;

  return Requete(requete);
}
vector<vector<string> > ProtocoleSQL::GetBright(int IdModule){
  string requete="select value from data where data.IdData='Bright' and data.IdModule=";
  requete+=itos(IdModule);
  requete+=";";
    cout<<requete<<endl;

  return Requete(requete);
}
vector<vector<string> > ProtocoleSQL::GetProgress(int IdModule){
  string requete="select value from data where data.IdData='Progression' and data.IdModule=";
  requete+=itos(IdModule);
  requete+=";";
    cout<<requete<<endl;

  return Requete(requete);
}

string ProtocoleSQL::itos(int i){
  ostringstream oss;
  oss<<i;
  return oss.str();
}
string ProtocoleSQL::btos(bool i){
  ostringstream oss;
  oss<<i;
  return oss.str();
}
