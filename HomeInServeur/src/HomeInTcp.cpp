#include "HomeInTcp.hpp"

HomeInTCP::HomeInTCP(){
  NbTramesRx=0;
  TailleAdresseClient=0;
  HomeIn_sizeServerAddress=0;
  HomeIn_portNumber=0;
  HomeIn_socket=0;
}

HomeInTCP::HomeInTCP(int port){
  NbTramesRx=0;
  TailleAdresseClient=0;
  HomeIn_sizeServerAddress=0;
  HomeIn_portNumber=port;
  HomeIn_socket=0;
}

void HomeInTCP::HITCP_Set_port(int port){
  HomeIn_portNumber=port;
}

int HomeInTCP::HITCP_Create_socket(){
  HomeIn_socket=socket(AF_INET,SOCK_STREAM,0);
  if(HomeIn_socket==-1)
    return -1;
  HomeIn_serverAddress.sin_family=AF_INET;
  HomeIn_serverAddress.sin_port=htons(HomeIn_portNumber);
  HomeIn_serverAddress.sin_addr.s_addr=INADDR_ANY;
  HomeIn_sizeServerAddress=sizeof(HomeIn_serverAddress);
  return 0;
}

int HomeInTCP::HITCP_Get_Descripteur_socket(){
  return HomeIn_socket;
}
	
int HomeInTCP::HITCP_Named_socket(){
  if(bind(HomeIn_socket,(struct sockaddr*)&HomeIn_serverAddress,HomeIn_sizeServerAddress))
    return -1;
  else
    return 0;
}

int HomeInTCP::HITCP_Listened_socket(){
  if(listen(HomeIn_socket,HomeIn_simultaneousClients))
    return -1;
  else	
    return 0;
}

void HomeInTCP::HITCP_Set_simultaneousClients(int SimClients){
  HomeIn_simultaneousClients=SimClients;
}

int HomeInTCP::HITCP_Have_Client(){
  cout<<"have client"<<endl;
  TailleAdresseClient=sizeof(AdresseClient);
  fdClient=accept(HomeIn_socket,(struct sockaddr*)&AdresseClient,(socklen_t*)&TailleAdresseClient);
  cout<<"have client"<<endl;

  if(fdClient<0){
    return -1;
  }
  else 
    return fdClient;
}

void HomeInTCP::HITCP_Send(const char * BufferTx,int fd){
  send(fd,BufferTx,strlen(BufferTx),0);
}
  
const char * HomeInTCP::HITCP_Recv(int fd){
  memset (BufferRx, 0, sizeof (BufferRx));
  if(recv(fd,BufferRx,255,0)<=0){
    close(fd);
    return "-1";
  }
  else 
    return BufferRx;
}

int HomeInTCP::Client(){
}
