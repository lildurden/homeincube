#include "ProtocolePLC.hpp"

ProtocolePLC::ProtocolePLC(RS232 * Serial){
  initCommande();
  SerialPLC=Serial;
}

bool ProtocolePLC::TxRS232(){
  return SerialPLC->TxTrame(TrameTX);
}	
bool ProtocolePLC::RxRS232(){
  return SerialPLC->RxTrame(TrameRX);
}
void ProtocolePLC::initCommande(){

  STX=0x02;
  Length=0x05;
  ETX=0x03;
	
  AllUnitsOFF=0x20;
  AllLtsOn=0x21;
  On=0x22;
  Off=0x23;
  Dim=0x24;
  Bright=0x25;
  AllLightsOff=0x26;
  AllUserltsOn=0x27;
  AllUserUnitOff=0x28;
  AllUserLightOff=0x29;
  Blink=0x2A;
  FadeStop=0x2B;
  PresetDin=0x2C;
  StatusOn=0x2D;
  StatusOff=0x2E;
  StatusReq=0x2F;
	
}

//definition des partie de la trame TX
void ProtocolePLC::InsertUserCode(unsigned char usrcd){
  UsrCode=usrcd;
  
}
void ProtocolePLC::InsertHomeUnit(unsigned char HomeUnit){
  Home_Unit=HomeUnit;
  // printf("Home_Unit: %02x\n",Home_Unit);
}
void ProtocolePLC::InsertCommand(unsigned char Command){
  Cmd=Command;
}
void ProtocolePLC::InsertData1(unsigned char Dat1){
  Data1=Dat1;
}
void ProtocolePLC::InsertData2(unsigned char Dat2){
  Data2=Dat2;
}
void ProtocolePLC::SetTrameTx(){
  TrameTX[0]=STX;
  TrameTX[1]=Length;
  TrameTX[2]=UsrCode;
  TrameTX[3]=Home_Unit;
  TrameTX[4]=Cmd;
  TrameTX[5]=Data1;
  TrameTX[6]=Data2;
  TrameTX[7]=ETX;
}
bool ProtocolePLC::ChkHome(unsigned char Home){
  if(Home<'A'||Home>'P')
    return false;
  else return true;
}
bool ProtocolePLC::ChkUnit(int Unit){
  if(Unit<1||Unit>16)
    return false;
  else return true;
}
bool ProtocolePLC::DefHomeUnit(unsigned char Hom,int Uni){
  if(!ChkHome(Hom)||!ChkUnit(Uni))
    return false;
  else{
    unsigned char Home=0,Unit=0x00,Final=0;
    switch(Hom){
    case 'A': 
      Home=0x00;
      break;
    case 'B': 
      Home=0x10;
      break;
    case 'C': 
      Home=0x20;
      break;
    case 'D': 
      Home=0x30;
      break;
    case 'E': 
      Home=0x50;
      break;
    case 'F': 
      Home=0x50;
      break;
    case 'G': 
      Home=0x60;
      break;
    case 'H': 
      Home=0x70;
      break;
    case 'I': 
      Home=0x80;
      break;
    case 'J': 
      Home=0x90;
      break;
    case 'K': 
      Home=0xA0;
      break;
    case 'L': 
      Home=0xB0;
      break;
    case 'M': 
      Home=0xC0;
      break;
    case 'N': 
      Home=0xD0;
      break;
    case 'O': 
      Home=0xE0;
      break;
    case 'P': 
      Home=0xF0;
      break;
    }
    Unit=Uni-1;
		
    Final=Final|Home;
    Final=Final|Unit;
    //Final=Final&0xFF;
    InsertHomeUnit(Final);
		
     cout<<Hom<<endl;
     cout<<Uni<<endl;
    printf(" %02x\n",Home);
    printf(" %02x\n",Unit);
    printf(" %02x\n",Final);
		
    return true;
  }
}
unsigned char* ProtocolePLC::GetTrameTX(){
  return TrameTX;
}
void ProtocolePLC::SetCommande(int commande){
  switch (commande) {
  case 1:
    InsertCommand(AllUnitsOFF);
    break;
  case 2:
    InsertCommand(AllLtsOn);
    break;
  case 3:
    InsertCommand(On);
    break;
  case 4:
    InsertCommand(Off);
    break;
  case 5:
    InsertCommand(Dim);
    //Data1 need
    break;
  case 6:
    InsertCommand(Bright);
    //Data1 need
    break;
  case 7:
    InsertCommand(AllLightsOff);
    break;
  case 8:
    InsertCommand(AllUserltsOn);
    break;
  case 9:
    InsertCommand(AllUserUnitOff);
    break;
  case 10:
    InsertCommand(AllUserLightOff);
    break;
  case 11:
    InsertCommand(Blink);
    //Data1 need
    break;
  case 12:
    InsertCommand(FadeStop);
    break;
  case 13:
    InsertCommand(PresetDin);
    //Data1 & Data2 need
    break;
  case 14:
    InsertCommand(StatusOn);
    //Data1 & Data2 need
    break;
  case 15:
    InsertCommand(StatusOff);
    break;
  case 16:
    InsertCommand(StatusReq);
    break;
  default:
    break;
  }
  //printf("commande: %02x\n",Cmd);
}
bool ProtocolePLC::defAll(unsigned char usrcd,unsigned char Home,int unit,int commande,unsigned char Dat1,unsigned char Dat2){
  printf(" trame av traitement %02x %02x %02x %02x %02x %02x\n", usrcd, Home, unit, commande, Dat1, Dat2);
  InsertUserCode(usrcd);
  //cout<<"test plc"<<endl;

  if(!DefHomeUnit(Home,unit))
    return false;
  //cout<<"test"<<endl;
  SetCommande(commande);
  InsertData1(Dat1);
  InsertData2(Dat2);
  //definition de la trame TX
  SetTrameTx();
  printf(" %02x %02x %02x %02x %02x %02x %02x %02x\n",TrameTX[0],TrameTX[1],TrameTX[2],TrameTX[3],TrameTX[4],TrameTX[5],TrameTX[6],TrameTX[7]);
  TxRS232(); 
  //RxRS232(); 
  //sleep(2);
  //RxRS232();
  /* int cpt=0;
  if(Cmd!=0x2c){
    while(TrameRX[4]!=Cmd||TrameRX[3]!=Home_Unit||cpt<3){
      usleep(50000);
      RxRS232();
      cpt++;
    }
  }
  else{
    //sleep(1);
    RxRS232();
    }*/
}
unsigned char * ProtocolePLC::GetTrameRX(){
  return TrameRX;
}
bool ProtocolePLC::checkFeedBack(){
  //sleep(1);
  //RxRS232();
  if(TrameRX[4]==Cmd&&TrameRX[3]==Home_Unit)
    return true;
  else return false;
}
