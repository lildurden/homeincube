#include "SQLite.hpp"

SQLite::SQLite(const char * nomdb){
  dbQuiContientTout=NULL;
  if(Open(nomdb)){
    cerr<<"marche pas la bdd"<<endl;
    Close();
  }
}
SQLite::~SQLite(){
  sqlite3_close(dbQuiContientTout);
}
int SQLite::Open(const char * nomdb){
  return sqlite3_open(nomdb, &dbQuiContientTout);
}

vector<vector<string> > SQLite::query(const char* query){
	
  vector<vector<string> > results;
  //cout<<"caca "<<query<<endl;
  sqlite3_stmt *statement=0;
 
  
  const char * fail=0;
  if(query){
	
    if(sqlite3_prepare_v2(dbQuiContientTout, query, -1, &statement, NULL) == SQLITE_OK){
      //cout<<"SQLite cpp before before while "<<endl;
      //sqlite3_bind_text(statement, 1, query, -1, SQLITE_TRANSIENT);
      int cols = sqlite3_column_count(statement);
      int result = 0;
      //cout<<"SQLite cpp before while"<<endl;
      while(true){
	result = sqlite3_step(statement);
	
	if(result == SQLITE_ROW)
	  {
	    vector<string> values;
	    for(int col = 0; col < cols; col++)
	      {
		string  val;
		char * ptr = (char*)sqlite3_column_text(statement, col);

		if(ptr)
		  {
		    val = ptr;
		  }
		else val = ""; // this can be commented out since std::string  val;
		// initialize variable 'val' to empty string anyway

		values.push_back(val);  // now we will never push NULL
	      }
	    results.push_back(values);
	  }
	else
	  {
	    break;  
	  }


      }
      
      sqlite3_finalize(statement);
    }
    
    string error = sqlite3_errmsg(dbQuiContientTout);
    if(error != "not an error") cout << query << " " << error << endl;
  }
  //sqlite3_finalize(statement);
  statement=0;
	

  //cout<<"ta maman la bdd"<<endl;
  return results; 

}

int SQLite::Close(){
  sqlite3_close(dbQuiContientTout);
}
