#include "HomeInTcp_Server.hpp"

HomeInTcp_Server::HomeInTcp_Server(){
  HomeInConnexion=new HomeInTCP();
}

HomeInTcp_Server::HomeInTcp_Server(int port){
  HomeInConnexion=new HomeInTCP(port);
}

int HomeInTcp_Server::HITCP_Server_init(int nbclient){
  HomeInConnexion->HITCP_Set_simultaneousClients(nbclient);
  if(HomeInConnexion->HITCP_Create_socket()<0)
    return -1;
  HomeIn_Socket=HomeInConnexion->HITCP_Get_Descripteur_socket();
  if(HomeInConnexion->HITCP_Named_socket()<0)
    return -5;
  if(HomeInConnexion->HITCP_Listened_socket()<0)
    return -10;
}

int HomeInTcp_Server::HITCP_Server_init(int nbclient,int port){
  HomeInConnexion->HITCP_Set_port(port);
  HomeInConnexion->HITCP_Set_simultaneousClients(nbclient);
  if(HomeInConnexion->HITCP_Create_socket()<0)
    return -1;
  HomeIn_Socket=HomeInConnexion->HITCP_Get_Descripteur_socket();
  if(HomeInConnexion->HITCP_Named_socket()<0)
    return -5;
  if(HomeInConnexion->HITCP_Listened_socket()<0)
    return -10;
}

int HomeInTcp_Server::HITCP_Server_letsGo(){
    cout<<"Lets Go"<<endl;
    HomeIn_FdClient=HomeInConnexion->HITCP_Have_Client();
    cout<<"fdclient"<<endl;
    if(HomeIn_FdClient<0){
      cerr<<"Erreur connexion client"<<endl;
      return -1;
    }
    cout<<"Connexion client acceptee"<<endl;    
	return HomeIn_FdClient;
}
	

void HomeInTcp_Server::HITCP_Server_send(const char* trame,int fd){
  HomeInConnexion->HITCP_Send(trame,fd);
}

const char * HomeInTcp_Server::HITCP_Server_recv(int fd){
  return HomeInConnexion->HITCP_Recv(fd);
}
