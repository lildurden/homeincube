#### Common variables for Makefiles

## Directories
ROOT=$(shell pwd)
DIR_BIN=bin
DIR_SRC=src
DIR_OBJ=obj
DIR_LIB=lib
DIR_HDR=include
DIR_DOC=doc

## Compilation
GXX=g++
#GXXARM=arm-none-linux-gnueabi-g++ 
# Flags for linking
CFLAGS=-I $(ROOT)/$(DIR_HDR)
# Flags for objects
CFLAGS_OBJ=-I $(ROOT)/$(DIR_HDR) -O3 -Wall
# Flags for libraries objects
CFLAGS_DYNAMIC_LIBS_OBJ=-fPIC
# Flags for libraries linkins
CFLAGS_DYNAMIC_LIBS=-shared

