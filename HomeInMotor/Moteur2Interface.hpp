/*
 * Moteur2Interface.hpp
 *
 *  Created on: 5 janv. 2012
 *      Author: foxnounours
 */

#ifndef _MOTEUR2INTERFACE_HPP
#define _MOTEUR2INTERFACE_HPP

class Moteur2Interface {

public:

	Moteur2Interface();

	/**
	 * Connecte l'utilisateur et retourne reussie
	 */
	bool connect(const char* adresse, int port);

	/**
	 * Connecte l'utilisateur et retourne reussie
	 */
	bool connectUser(const char* nom, const char* mdp);

	/**
	 * Retourne le nombre de pieces
	 */
	int getNbPieces();

	/**
	 * Retourne l'id de la piece
	 */
	int getIdPiece(int indexPiece);

	/**
	 * Retourne le nom de la piece
	 */
	const char* getNomPiece(int indexPiece);

	/**
	 * Retourne le nombre de module de la piece
	 */
	int getNbModule(int indexPiece);

	/**
	 * Retourne l'id du module
	 */
	int getIdModule(int indexPiece, int indexModule);

	/**
	 * Retourne le nom du module
	 */
	const char* getNomModule(int indexPiece, int indexModule);

	/**
	 * Retourne le type du module
	 */
	int getTypeModule(int indexPiece, int indexModule);

	/**
	 * Retourne l'etat du module
	 */
	bool getEtatModule(int indexPiece, int indexModule);

	/**
	 * Modifie l'etat du module
	 */
	void setEtatModule(int indexPiece, int indexModule, bool etat);

	/**
	 * Retourne le bright de la lumiere
	 */
	int getBrightLumiere(int indexPiece, int indexLumiere);

	/**
	 * Modifie le bright de la lumiere
	 */
	void setBrightLumiere(int indexPiece, int indexLumiere, int bright);

	/**
	 * Retourne le dim de la lumiere
	 */
	int getDimLumiere(int indexPiece, int indexLumiere);

	/**
	 * Modifie le dim de la lumiere
	 */
	void setDimLumiere(int indexPiece, int indexLumiere, int dim);

	/**
	 * Retourne la progression du volet
	 */
	int getProgressionVolet(int indexPiece, int indexVolet);

	/**
	 * Modifie la progression du volet
	 */
	void setProgressionVolet(int indexPiece, int indexVolet, int progression);

};

#endif /* MOTEUR2INTERFACE_HPP_ */
