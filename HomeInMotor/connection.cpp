/*
 * connection.cpp
 *
 *  Created on: 5 janv. 2012
 *      Author: foxnounours
 */

#include "connection.hpp"
#include "TCP.hpp"

using namespace std;

Connection::Connection(string adresse, int port) {

  this->adresse = adresse;
  this->port = port;

}

bool Connection::connect() {

  return TCP::get()->connexion(adresse.c_str(), port);

}

