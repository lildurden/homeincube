// Protection contre la redefinition de la classe
#ifndef _MODULE_HPP
#define _MODULE_HPP

// On defini le int correspondant au type module à 0
#define TYPE_MODULE 0

// On inclue les librairies et les headers necessaires
#include <string>
#include <sstream>

/**
 * Represente un module. Classe à utiliser comme un classe abstraite
 */
class Module { 

private:
	int id;
	std::string nom;
	int idPiece;
	bool etat;

public :
	Module( int, std::string, int, bool); // id et idPiece
	virtual int getType() = TYPE_MODULE;
	int getId();
	std::string getNom();
	int getIdPiece();
	bool getEtat();
	virtual void setEtat(bool);
	Module & operator = (const Module &);
	virtual std::string toString();
	virtual ~Module();
};

#endif
