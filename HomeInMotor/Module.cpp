// On inclue le header de la classe
#include "Module.hpp"
#include "Parser.hpp"

using namespace std;

Module::Module( int idC, string nom, int idPieceC, bool etatC){
	id = idC;
	this->nom = nom;
	idPiece = idPieceC;
	etat=etatC;
}
int Module::getId(){
	return id;
}
string Module::getNom() {
	return nom;
}

int Module::getIdPiece(){
	return idPiece;
}

bool Module::getEtat(){
	return etat;
}

void Module::setEtat( bool value){
	etat = value;
	Parser::get()->setModule(this);
}

Module & Module::operator = (const Module & source){
	etat=source.etat;
	id=source.id;
	idPiece=source.idPiece;
	return *this;
}

string Module::toString() {
	ostringstream s;
	s << "Module " << id << " : "
			<< nom << "est dans la piece "
			<< idPiece << " et sont état est "
			<< etat << endl;
	return s.str();
}

Module::~Module() {

}
