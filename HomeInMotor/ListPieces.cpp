#include "ListPieces.hpp"
#include "Parser.hpp"

using namespace std;

ListPieces* ListPieces::instance;

bool ListPieces::instancied = false;

string ListPieces::toString() {

  unsigned int i;
  string s = "Liste des pieces :\n";

  for(i=0; i < pieces.size(); i++) {

    s += pieces[i]->toString() + "\n";

  }

  return s;

}

ListPieces* ListPieces::get() {

  if(!instancied) {

    instance = new ListPieces();

  }
  
  return instance;
  
}

Piece* ListPieces::getByIndex(int index) {

  return pieces[index];

}

unsigned int ListPieces::getNbPiece() {

	return pieces.size();

}

ListPieces::ListPieces() {

    instancied = true;
    pieces = Parser::get()->getPieces();

}

void ListPieces::actualise() {

    instancied = true;
    pieces.clear();
    pieces = Parser::get()->getPieces();

}
