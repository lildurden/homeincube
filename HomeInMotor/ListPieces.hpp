// Protection contre la redefinition de la classe
#ifndef _LISTPIECES_HPP
#define _LISTPIECES_HPP

// On inclue les librairies et les headers necessaires
#include <vector>
#include <string>
#include "Piece.hpp"

/**
 * Classe qui contient l'ensemble des pieces.
 * Cette classe est un singleton.
 */
class ListPieces {

private :

	/**
	 * Attribut static qui contient l'unique instance de la liste des pieces
	 */
	static ListPieces* instance;

	/**
	 * Attribut static qui est a vrai si la liste des pieces a déjà été instancié
	 */
	static bool instancied;

	/**
	 * Attribut static qui contient la liste des pieces
	 */
	std::vector<Piece*> pieces;

	/*
	 * Constructeur de la liste des pieces
	 */
	ListPieces();

public :

	/**
	 * Methode static qui retourne l'unique instance de la liste des pieces
	 */
	static ListPieces* get();

	/**
	 * Methode qui retourne le nombre de piece
	 */
	unsigned int getNbPiece();

	/**
	 * Methode qui detourne la piece stocké à l'index passé en parametre
	 */
	Piece* getByIndex(int index);

	/**
	 * Methode qui recharge la liste des piece
	 */
	void actualise();

	/**
	 * Methode qui retourne une string representant la liste des pieces
	 */
	std::string toString();

};

#endif
