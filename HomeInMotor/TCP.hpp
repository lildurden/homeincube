#ifndef TCP_HPP
#define TCP_HPP
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <ctype.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <fcntl.h>
using namespace std;
class TCP{

private:
  int Port;
  char * adresse;
  int fd;
  struct sockaddr_in AdresseServeur;
  struct hostent* serveur;
  int Res;
  int SizeBufferTX;
  char BufferRX[255];
  char * BufferTX;

  int NbTramesRecues;
  int i;
  char add[20];
  char * ToSend;
  void clearBufferRX(){
    memset (BufferRX, 0, sizeof (BufferRX));
  }
  void clearBufferTX(){
    if(BufferTX)
      delete BufferTX;
    if(ToSend)
      delete ToSend;
  }
  static TCP* instance;
  static bool instancied;
  int typeTrame;
  TCP();

public: 
  ~TCP();
  static TCP * get();
  bool connexion(const char* adresse, int port);
  void sended(const char* trame);
  const char* recieve();
};


#endif
