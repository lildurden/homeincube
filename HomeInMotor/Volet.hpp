#ifndef _VOLET_HPP
#define _VOLET_HPP
#define TYPE_VOLET 3

#include <string>
#include "Module.hpp"

class Volet : public Module {

private:
	int progression;

public :
	Volet (int, std::string, int, bool, int);
	int getProgression();
	void setProgression(int);
	virtual int getType();
	virtual std::string toString();
	virtual ~Volet();
};

#endif
