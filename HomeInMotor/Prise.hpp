#ifndef _PRISE_HPP
#define _PRISE_HPP
#define TYPE_PRISE 1

#include <string>
#include "Module.hpp"

class Prise : public Module {
public :
	Prise(int , std::string, int , bool);
	virtual int getType();
	virtual std::string toString();
	virtual ~Prise();

};

#endif
